/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  device.h                                                        */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Device specific main header file to be included in all source files.      */
/*                                                                           */
/*===========================================================================*/
#ifndef   DEVICE_H
#define   DEVICE_H

/*===========================================================================*/
/* Global compiler definition */
/*===========================================================================*/ 
#define COMP_GHS     1
#define COMP_IAR     2


#if defined (__IAR_SYSTEMS_ASM__)
    #define COMPILER COMP_IAR
#elif defined (__IAR_SYSTEMS_ICC__)
    #define COMPILER COMP_IAR
#else /*GHS */
    #define COMPILER COMP_GHS
#endif



/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#if COMPILER == COMP_GHS
	#include <v800_ghs.h>
	#include "icu_feret_v3.h"
	#include "io_macros_v2.h"
	
	#include "dr7f701057.dvf.h"
#endif

#if COMPILER == COMP_IAR
	#include <intrinsics.h>
	#include <ior7f701057.h>
#endif



/*===========================================================================*/
/* Defines */
/*===========================================================================*/
/* Function for the write-sequence of protected registers */
#define protected_write(preg,pstatus,reg,value)   do{\
                                                  (preg)=0xa5u;\
                                                  (reg)=(value);\
                                                  (reg)=~(value);\
                                                  (reg)=(value);\
                                                  }while((pstatus)==1u)
                                          
#if COMPILER == COMP_IAR
  #define __EI() __enable_interrupt()
  #define __DI() __disable_interrupt()
  typedef unsigned char     u08_T;
  typedef unsigned short    u16_T;
  typedef unsigned long     u32_T;
#endif


typedef signed char       s08_T;
typedef signed short      s16_T;
typedef signed int        s32_T;

                                                    
/****************************************************************************/
/* The IAR needs two different called main functions for each core while    */
/* the GHS needs a "main()"-function in each core-project.                  */
/* Thus the "main_peX"-functions are both executed as "main" when using GHS */
/****************************************************************************/


#endif  /* DEVICE_H */
