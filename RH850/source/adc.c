/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  adc.c                                                           */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the ADC Functions                                         */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "adc.h"

/*===========================================================================*/
/* Functions */
/*===========================================================================*/

/******************************************************************************
** Function:    INTADCA0ERR
** Description: Error interrupt is generated when the PWM-Diagnostic of the HPLEDs
**              is out of expected range. It creates a call back in the tasks source
** Parameter:   None
** Return:      None
******************************************************************************/
#if COMPILER == COMP_IAR
  #pragma vector = 0x002F
#endif
__interrupt void INTADCA0ERR(void)
{
  ADCA0_error_cb(ADCA0ULER);   /* Upper/Lower Error information is given to the error call back function */
  ADCA0ECR = 0x08u;            /* Set the ULEC bit of the ErrorClearRegister of ADCA0 to clear the upper/lower limit error flag */
}

/******************************************************************************
** Function:    AP_init
** Description: This function initializes the analogue ports for ADC/PWSA usage
** Parameter:   None
** Return:      None
******************************************************************************/
void AP_init(void)
{
  APM0 |= 1u<<1;   /* AP0_1 SpecialFunct(PWGADIN Ch1) */
  APM0 |= 1u<<2;   /* AP0_2 SpecialFunct(PWGADIN Ch2) */
  APM0 |= 1u<<0;   /* AP0_0 SpecialFunct(ADCA0) */
  APM1 |= 1u<<0;   /* AP1_0 SpecialFunct(ADCA1) */
}


/******************************************************************************
** Function:    AP_deinit
** Description: This function deinitializes the analogue ports for ADC/PWSA usage
** Parameter:   None
** Return:      None
******************************************************************************/
void AP_deinit(void)
{
  APM0 &= ~(1u<<1);   /* AP0_1 SpecialFunct(PWGADIN Ch1) */
  APM0 &= ~(1u<<2);   /* AP0_2 SpecialFunct(PWGADIN Ch2) */
  APM0 &= ~(1u<<0);   /* AP0_0 SpecialFunct(ADCA0) */
  APM1 &= ~(1u<<0);   /* AP1_0 SpecialFunct(ADCA1) */
}

/******************************************************************************
** Function:    ADC_interrupt_enable
** Description: This function enables the error table interrupt for ADCA0/ADCA1
** Parameter :  None
** Return:      None
******************************************************************************/
void ADC_interrupt_enable(void)
{
  /* ErrorInterrupt for ADCA0 enabled */
  TBADCA0ERR = 1u;      /* Error table interrupt is enabled by setting the table bit to 1 */
  MKADCA0ERR = 0u;      /* Error interrupt is unmasked by setting the mask bit to 0 */

  /* Error Interrupt for ADCA1 enabled */
  TBADCA1ERR = 1u;      /* Error table interrupt is enabled by setting the table bit to 1 */
  MKADCA1ERR = 0u;      /* Error interrupt is unmasked by setting the mask bit to 0 */
}

/******************************************************************************
** Function:    ADC_interrupt_disable
** Description: This function disables the error table interrupt for ADCA0/ADCA1
** Parameter:   None
** Return:      None
******************************************************************************/
void ADC_interrupt_disable(void)
{
  /* Error TableInterrupt for ADCA0 disabled */
  TBADCA0ERR = 0u;      /* Error table interrupt is disabled by setting the table bit to 0 */
  MKADCA0ERR = 1u;      /* Error interrupt is masked by setting the mask bit to 1 */

  /* Error TableInterrupt for ADCA1 disabled */
  TBADCA1ERR = 1u;      /* Error table interrupt is disabled by setting the table bit to 0 */
  MKADCA1ERR = 0u;      /* Error interrupt is masked by setting the mask bit to 1 */
}

/******************************************************************************
** Function:    ADC_channel_init
** Description: This function initializes ADC-Function for the Potentiometers
**              and PWM diagnostic function
** Parameter:   None
** Return:      None
******************************************************************************/
void ADC_channel_init(void)
{
  /* ADCA0 */
  ADCA0VCR00 = 0x00u;     /* Virtual Channel 0 of ADCA0 is linked to physical channel ADCA0I0 */
                          /* Upper/lower limit not checked / No scan group interrupt is output */
  
  ADCA0ADCR = 0x02u;      /* Asynchronous suspend / 12bit mode / PWDDR and ADCA0DR00 are set to right align */
                          /* The self-diagnostic voltage circuit is turned off */

  ADCA0SMPCR = 0x12u;     /* Set sampling time to 18*1/40MHz = 0.45us */
  ADCA0SFTCR = 0x08u;     /* Upper/Lower limit error enabled / DR/DIR registers are not cleared when read */
  ADCA0SGCR1 = 0x10u;     /* ScanGroup interrupt is output when scan ends */
  
  /* ScanGroup start/end registers */
  ADCA0SGVCSP1 = 0x00u;   /* ScanGroup starts at virtual channel 0 */
  ADCA0SGVCEP1 = 0x00u;   /* ScanGroup ends at virtual channel 0 */

  /* ADCA1 */
  ADCA1VCR00 = 0x100u;    /* Virtual Channel 0 of ADCA1 is linked to physical channel ADCA1I0 */
                          /* Upper/lower limit not checked / No scan group interrupt is output */

  ADCA1ADCR = 0x02u;      /* Asynchronous suspend / 12bit mode / ADCA1DR00 is set to right align */
                          /* The self-diagnostic voltage circuit is turned off */

  ADCA1SMPCR = 0x12u;     /* Set sampling time to 18*1/40MHz = 0.45us */
  ADCA1SFTCR = 0x08u;     /* Upper/Lower limit error enabled / DR/DIR registers are not cleared when read */
  ADCA1SGCR2 = 0x10u;     /* ScanGroup interrupt is output when scan ends */
  
  /* ScanGroup start/end registers */
  ADCA1SGVCSP2 = 0x00u;   /* ScanGroup starts at virtual channel 0 */
  ADCA1SGVCEP2 = 0x00u;   /* ScanGroup ends at virtual channel 0 */
}

/******************************************************************************
** Function:    ADC_read
** Description: This function reads the actual status of the Potentiometers
** Parameter:   ADC Channel 0
**                          1
** Return:      ADCA0DIR00
**              ADCA1DIR00
******************************************************************************/
u16_T ADC_read(u16_T adc_channel)
{
  u16_T return_value = 0u;
  switch(adc_channel)
  {
    /* Channel 0*/
    case 0u:
      ADCA0SGSTCR1 = 1u;                          /* Conversion start trigger */
      while(RFADCA0I0 == 0u){}                    /* Wait for conversion end interrupt request */
      RFADCA0I0 = 0u;                             /* Reset interrupt request */
      return_value = (u16_T) ADCA0DIR00&0xffffu;  /* Return the lower 16bit which are the read value */
    break;
    
    /* Channel 1 */
    case 1:
      ADCA1SGSTCR2 = 1u;                          /* Conversion start trigger */
      while(RFADCA1I1 == 0u){}                    /* Wait for conversion end interrupt request */
      RFADCA1I1 = 0u;                             /* Reset interrupt request */
      return_value = (u16_T) ADCA1DIR00&0xffffu;  /* Return the lower 16bit which are the read value */
    break;
    
    default:
    break;
  }
  return return_value;
}

/******************************************************************************
** Function:    ADC_LPS_init
** Description: This function initializes ADC for LPS usage
** Parameter:   None
** Return:      None
******************************************************************************/
void ADC_LPS_init(void)
{
  ADCA0VCR00 = 0x40u;      /* Virtual Channel 0 of ADCA0 is linked to physical channel ADCA0I4 (LPS) / Limits checked in ADCA0ULLMTBR0 */
  ADCA0ECR = 0x0cu;        /* Set the ULEC bit of the ErrorClearRegister of ADCA1 to clear the upper/lower limit error flag */
  
  ADCA0SGCR1 = 0x00u;      /* Hardware trigger disabled / No interrupt is output when scan ends */
  ADCA0SFTCR = 0x08u;      /* Upper/Lower limit interrupt enabled */
  ADCA0SMPCR = 0x12u;      /* ADC Sampling Control Setting */
  ADCA0ADCR = 0x00u;       /* ADC Common Operation Control Setting */
  ADCA0SGTSEL1 = 0x10u;    /* LPS as hardware trigger selected */
  ADCA0SGCR1 = 0x11u;      /* INT_SGx is output when the scan for SGx ends / Hardware trigger enabled */
}

/******************************************************************************
** Function:    ADCA0_limit
** Description: This function sets the upper/lower limit of the ADCA0 channel
** Parameter:   Limit Channel:
**                0: LPS-Limit used for WakeUp in DEEPSTOP
**                1: PWSA-Limit for HPLED1
**                2: PWSA-Limit for HPLED2
**              Upper Limit[0-4095]
**              Lower Limit[0-4095]
** Return:      None
******************************************************************************/
void ADCA0_limit(u16_T limit_channel, u16_T upper, u16_T lower)
{
  /* Select the upper and lower limit values */
  switch(limit_channel)
  {
    /* Limit registers: [31:20] Upper limit, [15:4] Lower limit */
    case 0u:
      ADCA0ULLMTBR0 =  ((u32_T)upper << 20);   /* Set upper limit */
      ADCA0ULLMTBR0 |=  ((u32_T)lower << 4);   /* Set lower limit */
    break;
    
    case 1u:
      ADCA0ULLMTBR1 =  ((u32_T)upper << 20);   /* Set upper limit */
      ADCA0ULLMTBR1 |=  ((u32_T)lower << 4);   /* Set lower limit */
    break;
    
    case 2u:
      ADCA0ULLMTBR2 =  ((u32_T)upper << 20);   /* Set upper limit */
      ADCA0ULLMTBR2 |=  ((u32_T)lower << 4);   /* Set lower limit */
    break;
    
    default:
    break;
  }
  ADCA0ECR = 0x08u;    /* Set the ULEC bit of the ErrorClearRegister of ADCA0 to clear the upper/lower limit error flag */
}