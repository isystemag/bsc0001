/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  button.c                                                        */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the button functions                                      */
/*                                                                           */
/*===========================================================================*/


/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "button.h"

/*===========================================================================*/
/* Defines */
/*===========================================================================*/
#define BUTTON_PRESSED_SHORT      10u    /* 10ms for short press */
#define BUTTON_PRESSED_LONG     3000u    /* 3s for long press */

/*===========================================================================*/
/* Variables */
/*===========================================================================*/
static u32_T button_P0_9_count=0u;
static enum button_states button_P0_9_state=button_state_none;


/*===========================================================================*/
/* Functions */
/*===========================================================================*/

/*****************************************************************************
** Function:    button_init
** Description: GPI initialization for button Pin
** Parameter:   None
** Return:      None
******************************************************************************/
void button_init(void){
  /* Configures P0_9 as general purpose input */
  PMC0 &= ~(1u<<9);
  PM0 |= (1u<<9);
  PIBC0 |= (1u<<9);  /* Enable input port buffer for P0_9 */
}

/*****************************************************************************
** Function:    button_handler
** Description: Checks the status and counts the time the button is pressed
** Parameter:   None
** Return:      None
******************************************************************************/
void button_handler(void)
{
  u16_T current_pinstatus;
  enum button_states return_value=button_state_none;
  
  /* Read current pin status from port buffer */
  current_pinstatus=PPR0;
  
  /* Check if button P0_9 is pressed */
  if((current_pinstatus&(1u<<9)) == 0u )
  {
    /* Button is pressed */
    if(button_P0_9_count >= BUTTON_PRESSED_LONG)
    {
      return_value = button_state_long;
    }
    
    button_P0_9_count++;
  }
  else
  {
    /* Button is not pressed */
    if(((button_P0_9_count > BUTTON_PRESSED_SHORT) && (button_P0_9_count < BUTTON_PRESSED_LONG)) == 1)
    {
      return_value = button_state_short;
    }
    
    button_P0_9_count = 0u;
    
  }
  
  /* Save last button event to global variable */
  button_P0_9_state=return_value;
}


/*****************************************************************************
** Function:    button__P0_9_status
** Description: This functions returns the last button event
** Parameter:   None
** Return:      button_states : last button event
******************************************************************************/
enum button_states button_P0_9_status(void)
{
  enum button_states return_value;
  
  return_value=button_P0_9_state;
  
  button_P0_9_state=button_state_none;
  
  return return_value;
}


/*****************************************************************************
** Function:    button__P0_9_read
** Description: This functions reads the actual button status
** Parameter:   None
** Return:      button status
******************************************************************************/
u16_T button_P0_9_read(void)
{
  return (PPR0&(1u<<9))>>9;
}

/*****************************************************************************
** Function:    INTP12_init
** Description: This functions configures the button as interrupt source
** Parameter:   None
** Return:      None
******************************************************************************/
void INTP12_init(void)
{
  /* Setting P0_9 as alternative interrupt input */
  PIBC0 &= ~(1u<<9);
  PMC0 |= (1u<<9);
  PM0 |= (1u<<9);
  PFCAE0 &= ~(1u<<9);
  PFCE0 &= ~(1u<<9);
  PFC0 &= ~(1u<<9);

  /* Filter setting */
  FCLA0CTL4_INTPH = 0x01u;   /* Interrupt is generated on falling edge */
}
