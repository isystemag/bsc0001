/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  clocks.c                                                        */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the clock configurations                                  */
/*                                                                           */
/*===========================================================================*/


/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "clocks.h"                 

/*===========================================================================*/
/* Functions */
/*===========================================================================*/
/*****************************************************************************
** Function:    init_clocks
** Description: This function generated the 120MHz Main Clock and enabled the SubOsc
** Parameter:   None
** Return:      None
******************************************************************************/
void init_clocks(void)
{ 
    /* Prepare 16MHz MainOsz */
  if((MOSCS&0x04u) != 0x4u)                       /* Check if MainOsc needs to be started */
  {
   MOSCC=0x06;                                    /* Set MainOSC gain (8MHz < MOSC frequency =< 16MHz) */
   MOSCST=0x8000;                                 /* Set MainOSC stabilization time to max. (8,19 ms) */
   protected_write(PROTCMD0,PROTS0,MOSCE,0x01u);  /* Trigger Enable (protected write) */
   while ((MOSCS&0x04u) != 0x04u);                /* Wait for active MainOSC */
  }

  if((PLLS&0x04u) != 0x04u)                       /* Check if PLL needs to be started */
  {
    /* Prepare PLL*/
    PLLC=0x00000a27u;                             /* 16 MHz MainOSC -> 80MHz PLL */
    protected_write(PROTCMD1,PROTS1,PLLE,0x01u);  /* Enable PLL */
    while((PLLS&0x04u) != 0x04u){}                /* Wait for active PLL */
  }

  /* Set Peripheral CLK2 to 40 MHZ (PPLL2) */
  protected_write(PROTCMD1,PROTS1,CKSC_IPERI2S_CTL,0x02u);
  while(CKSC_IPERI2S_ACT!=0x02u);
  
  /* Select the PPLLCLK2 as ADCA0 Clock  */  
  protected_write(PROTCMD0,PROTS0,CKSC_AADCAS_CTL,0x03u);
  while(CKSC_AADCAS_ACT!=0x03u);
 
  /* Select the PPLLCLK2 as RLIN Clock */
  protected_write(PROTCMD1,PROTS1,CKSC_ILINS_CTL,0x03u);
  while(CKSC_ILINS_ACT!=0x03u);
 
  /* Main Osc -> CAN */
  protected_write(PROTCMD1,PROTS1,CKSC_ICANS_CTL,0x02u);
  while (CKSC_ICANS_ACT !=0x02u);
   
  protected_write(PROTCMD1,PROTS1, CKSC_ICANOSCD_CTL,0x01u);
  while ( CKSC_ICANOSCD_CTL !=0x01u);
 
  /* CPU Clock divider = PLL0/1 */
  protected_write(PROTCMD1,PROTS1,CKSC_CPUCLKD_CTL,0x01u);
  while(CKSC_CPUCLKD_ACT!=0x01u);

  /* PLL0 -> CPU Clock */ 
  protected_write(PROTCMD1,PROTS1,CKSC_CPUCLKS_CTL,0x03u);
  while(CKSC_CPUCLKS_ACT!=0x03u);
  
  /* Select the Low Speed IntOsc(240kHz) as TAUJ Clock */  
  protected_write(PROTCMD0,PROTS0,CKSC_ATAUJS_CTL,0x04u);
  while(CKSC_ATAUJS_CTL!=0x04u);
  
  /* Select the PPLLCLK2 as ADCA1 Clock  */  
  protected_write(PROTCMD1,PROTS1,CKSC_IADCAS_CTL,0x03u);
  while(CKSC_IADCAS_ACT!=0x03u);
}

void init_DEEPSTOP_clocks(void)
{
  /* Select HighSpeed Internal OSC for ADC0 */
  protected_write(PROTCMD0,PROTS0,CKSC_AADCAS_CTL,0x01u);
  while(CKSC_AADCAS_ACT!=0x01u);
  
  /* ADCA0-CLK is not stopped in stand-by */
  CKSC_AADCAD_STPM=0x01u;
  
  /* Select the Low Speed IntOsc(240kHz) as TAUJ Clock */  
  protected_write(PROTCMD0,PROTS0,CKSC_ATAUJS_CTL,0x03u);
  while(CKSC_ATAUJS_CTL!=0x03u);
  
  /* The TAUJ Clock is not stopped in stand by */
  CKSC_ATAUJD_STPM = 0x01u;
}
