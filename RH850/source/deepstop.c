/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  deepstop.c                                                      */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the DeepStop Functions                                    */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "deepstop.h"

/*===========================================================================*/
/* Functions */
/*===========================================================================*/
 
/*****************************************************************************
** Function:    prepare_WakeUp
** Description: This function resets all resets and WakeUps and set the 
**              enabled WakeUp factors
** Parameter:   None
** Return:      None
******************************************************************************/
void prepare_WakeUp(void)
{
  /* Configure valid wake-up conditions */
  WUFMSK0 = ~((1u<<23)|(1u<<14)|(1u<<19));   /* Enable Wake up INTP12/SEQ1/SEQ2 (Button/LPS(analogue and digital) */
  WUFMSK20 = 0xfffffffu;                     /* No other Wake ups enabled */
  WUFMSK_ISO0 = 0xfffffffu;                  /* No other Wake ups enabled */
  
  /* WakeUpFactor RegisterReset */
  WUFC0 = 0xffffffffu;
  WUFC20 = 0xfffffffu;
  WUFC_ISO0 = 0xfffffffu;
  
  /* Clear all ResetFactor Flags */
  RESFC = 0x000007ffu;
}

/*****************************************************************************
** Function:    enter_DeepStop
** Description: This function enters the DeepStop
** Parameter:   None
** Return:      None
******************************************************************************/
void enter_DeepStop(void)
{
    protected_write(PROTCMD0,PROTS0,STBC0PSC,0x02u);   /* Enter DeepStopMode */
    while(1){}
}

/*****************************************************************************
** Function:    get_wakeup_factor
** Description: This function returns the factor that caused a wakeup
** Parameter:   None
** Return:      WUF0 - WakeUpFactor register
******************************************************************************/
u32_T get_wakeup_factor(void)
{
    return WUF0;
}

/*****************************************************************************
** Function:    release_ioHold
** Description: This function releases the IO Hold function of ISO ports
** Parameter:   None
** Return:      None
******************************************************************************/
void release_ioHold(void)
{
  /* Release I/O-Hold Buffer */
  protected_write(PROTCMD0,PROTS0,IOHOLD,0x00u);
  while(IOHOLD!=0x00u){}
}
