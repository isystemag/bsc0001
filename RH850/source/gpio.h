/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  gpio.h                                                          */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Header file for the gpio.c source code                                    */
/*                                                                           */
/*===========================================================================*/
#ifndef _GPIO_H
#define _GPIO_H

/*===========================================================================*/
/* Defines */
/*===========================================================================*/
enum GPIO_Name {     GPIO_LED1=0, 
                                    GPIO_LED2,
                                    GPIO_APO,
                                    GPIO_DPO,
                                    GPIO_DIN,
                                    GPIO_SELDP0,
                                    GPIO_SELDP1,
                                    GPIO_SELDP2,
                                    GPIO_INTP
                                }; 
 
struct GPIO_pin {
    enum GPIO_Name Name;
    u16_T PinNumber;
    volatile u16_T * PMC_Reg;
    volatile u16_T * PM_Reg;
    volatile const u16_T * PPR_Reg;
    volatile u16_T * PIBC_Reg;
    volatile u16_T * P_Reg;
};

/*===========================================================================*/
/* Function defines */
/*===========================================================================*/
void GPIO_init_output(enum GPIO_Name PortPin);
void GPIO_init_input(enum GPIO_Name PortPin);
void GPIO_output_off(enum GPIO_Name PortPin);
void GPIO_output_on(enum GPIO_Name PortPin);
u16_T GPIO_input_read(enum GPIO_Name PortPin);
void GPIO_deinit(enum GPIO_Name PortPin);
#endif
