/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  lps.c                                                           */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the LowPowerSampler                                       */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "lps.h"
#include "gpio.h"

/*===========================================================================*/
/* Functions */
/*===========================================================================*/

/*****************************************************************************
** Function:    LPS_auto_init
** Description: Initializes the automatic functionality of the LPS
** Parameter:   None
** Return:      None
******************************************************************************/
void LPS_auto_init(void)
{
  /* P0_0 alternative function DPO */
  protected_write(PPCMD0,PROTS0,PODC0,0x00u);      /* Change P0_0 from Open-drain to Push-pull mode */
  while(PODC0!=0x00u){}
  PMC0 |= (1u<<0);
  PM0 &= ~(1u<<0);
  PFCAE0 &= ~(1u<<0);
  PFCE0 |= 1u<<0;
  PFC0 |= 1u<<0;
    
  /* P0_1 alternative function APO */
  PMC0 |= (1u<<1);
  PM0 &= ~(1u<<1);
  PFCAE0 &= ~(1u<<1);
  PFCE0 |= 1u<<1;
  PFC0 |= 1u<<1;
    
  /* P8_1 alternative function DIN */
  PMC8 |= (1u<<1);
  PM8 |= (1u<<1);
  PFCE8 &= ~(1u<<1);
  PFC8 |= 1u<<1;
    
  /* P0_4 alternative function SELDP0 */
  PMC0 |= (1u<<4);
  PM0 &= ~(1u<<4);
  PFCAE0 &= ~(1u<<4);
  PFCE0 |= 1u<<4;
  PFC0 &= ~(1u<<4);
    
  /* P0_5 alternative function SELDP1 */
  PMC0 |= (1u<<5);
  PM0 &= ~(1u<<5);
  PFCAE0 &= ~(1u<<5);
  PFCE0 &= ~(1u<<5);
  PFC0 |= 1u<<5;
    
  /* P0_6 alternative function SELDP2 */
  PMC0 |= (1u<<6);
  PM0 &= ~(1u<<6);
  PFCAE0 &= ~(1u<<6);
  PFCE0 &= ~(1u<<6);
  PFC0 |= 1u<<6;

  /* Set-up digital and analogue stabilization time */
  CNTVAL = 0xFFCFu;        /* AP0 stabilization time  = (1/8MHz high-speed IntOSC) � 16 � CNT(upper 8 Bits) = 0.51ms */
                           /* DP0 stabilization time  = (1/8MHz high-speed IntOSC) � 16 � CNT(lower 8 Bits) = 0.414ms */

  /* Configure one 8to1 digital Multiplexer connected to DPIN0 */
  DPSELR0 = 0x01u;         /* Enable D0EN_0 */
  DPSELRM = 0x01010101u;   /* Enable D1EN_0, D2EN_0, D3EN_0, D4EN_0 */
  DPSELRH = 0x00010101u;   /* Enable D5EN_0, D6EN_0, D7EN_0 */
    
  SCTLR = 0x73u;           /* Sequence Start Trigger is INTTAUJ0I0 / read 8 times / digital/analogue mode enabled */
    
  EVFR = 0u;               /* Clear event flag register */
}

/*****************************************************************************
** Function:    LPS_auto_deinit
** Description: Deinitializes the automatic functionality of the LPS
** Parameter:   None
** Return:      None
******************************************************************************/
void LPS_auto_deinit(void)
{
  /* Set P0_0 (DPO) to GPIO input mode */
  PMC0 &= ~(1u<<0);
  PM0 |= (1u<<0);
    
  /* P0_1 (APO) to GPIO input mode */
  PMC0 &= ~(1u<<1);
  PM0 |= (1u<<1);

  /* P8_1 (DIN) to GPIO input mode */
  PMC8 &= ~(1u<<1);
  PM8 |= (1u<<1);
  
  /* P0_4 (SELDP0) to GPIO input mode */
  PMC0 &= ~(1u<<4);
  PM0 |= (1u<<4);
    
  /* P0_5 (SELDP1) to GPIO input mode */
  PMC0 &= ~(1u<<5);
  PM0 |= (1u<<5);
    
  /* P0_6 (SELDP2) to GPIO input mode */
  PMC0 &= ~(1u<<6);
  PM0 |= (1u<<6);
    
  /* Disable LPS */
  SCTLR = 0u;    
}

/*****************************************************************************
** Function:    LPS_manual_init
** Description: Initializes the manual functionality of the LPS
** Parameter:   None
** Return:      None
******************************************************************************/
void LPS_manual_init(void)
{
  /* Change P0_0 from Open-drain to Push-pull mode */
  protected_write(PPCMD0,PROTS0,PODC0,0x00u);    
  while(PODC0!=0x00u){}
 
  /* DPO, APO, SELDPn as GPIO for manual use */
  GPIO_init_output(GPIO_DPO); 
  GPIO_init_output(GPIO_APO); 
  GPIO_init_output(GPIO_SELDP0);
  GPIO_init_output(GPIO_SELDP1); 
  GPIO_init_output(GPIO_SELDP2);
  GPIO_init_input(GPIO_DIN);
    
  /* Set DPO / APO to high */
  GPIO_output_on(GPIO_DPO);
  GPIO_output_on(GPIO_APO); 
}

/*****************************************************************************
** Function:    LPS_read_compare
** Description: Reads the actual status of the LPS Switch (S3) and writes it 
**              to the compare register
** Parameter:   None
** Return:      None
******************************************************************************/
void LPS_read_compare(void)
{
  /* This manually reads the actual setting of the LPS Digital Input */
   
  /* Reset compare registers */
  DPDSR0 = 0x00000000u;
  DPDSRM = 0x00000000u;
  DPDSRH = 0x00000000u; 
    
  GPIO_output_off(GPIO_SELDP0);   /* Configure manually the switch which is read */
  GPIO_output_off(GPIO_SELDP1);   /* SELDP     0 1 2                             */
  GPIO_output_off(GPIO_SELDP2);   /*          [X|X|X] -> 0-7                     */
  if((PPR8&0x02u)==2u)
  {
    DPDSR0 |= 1u;
  }
    
  GPIO_output_on(GPIO_SELDP0);
  GPIO_output_off(GPIO_SELDP1);
  GPIO_output_off(GPIO_SELDP2);
  if((PPR8&0x02u)==2u)
  {
    DPDSRM |= 1u;
  }
    
  GPIO_output_off(GPIO_SELDP0);
  GPIO_output_on(GPIO_SELDP1);
  GPIO_output_off(GPIO_SELDP2);
  if((PPR8&0x02u)==2u)
  {
    DPDSRM |= 1u<<8;
  }
    
  GPIO_output_on(GPIO_SELDP0);
  GPIO_output_on(GPIO_SELDP1);
  GPIO_output_off(GPIO_SELDP2);
  if((PPR8&0x02u)==2u)
  {
    DPDSRM |= 1u<<16;
  }
  
  GPIO_output_off(GPIO_SELDP0);
  GPIO_output_off(GPIO_SELDP1);
  GPIO_output_on(GPIO_SELDP2);
  if((PPR8&0x02u)==2u)
  {
    DPDSRM |= 1u<<24;
  }
    
  GPIO_output_on(GPIO_SELDP0);
  GPIO_output_off(GPIO_SELDP1);
  GPIO_output_on(GPIO_SELDP2);
  if((PPR8&0x02u)==2u)
  {
    DPDSRH |= 1u;
  }
    
  GPIO_output_off(GPIO_SELDP0);
  GPIO_output_on(GPIO_SELDP1);
  GPIO_output_on(GPIO_SELDP2);
  if((PPR8&0x02u)==2u)
  {
    DPDSRH |= 1u<<8;
  }
    
  GPIO_output_on(GPIO_SELDP0);
  GPIO_output_on(GPIO_SELDP1);
  GPIO_output_on(GPIO_SELDP2);
  if((PPR8&0x02u)==2u)
  {
    DPDSRH |= 1u<<16;
  }
}

/*****************************************************************************
** Function:    LPS_manual_deinit
** Description: Deinitializes the manual functionality of the LPS
** Parameter:   None
** Return:      None
******************************************************************************/
void LPS_manual_deinit(void)
{
  GPIO_output_off(GPIO_DPO);
  GPIO_output_off(GPIO_APO);
   
  /* DPO */
  protected_write(PPCMD0,PROTS0,PODC0,0x01u);
  while(PODC0!=0x01u){}
  PMC0 &= ~(1u<<0);
  PM0 &= ~(1u<<0);
  PFCAE0 &= ~(1u<<0);
  PFCE0 &= ~(1u<<0);
  PFC0 &= ~(1u<<0);
   
  /* APO */
  PMC0 &= ~(1u<<1);
  PM0 &= ~(1u<<1);
  PFCAE0 &= ~(1u<<1);
  PFCE0 &= ~(1u<<1);
  PFC0 &= ~(1u<<1);
  
  /* DIN */
  PMC8 &= ~(1u<<1);
  PM8 &= ~(1u<<1);
  PFCE8 &= ~(1u<<1);
  PFC8 &= ~(1u<<1);
  
  /* SELDP0 */
  PMC0 &= ~(1u<<4);
  PM0 &= ~(1u<<4);
  PFCAE0 &= ~(1u<<4);
  PFCE0 &= ~(1u<<4);
  PFC0 &= ~(1u<<4);
}
