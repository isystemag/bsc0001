/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  main.c                                                          */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Main file for the F1x StarterKit Sample Software                          */
/*                                                                           */
/*===========================================================================*/


/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
//#include "tasks.h"
//#include "button.h"
#include "clocks.h"
//#include "rlin30.h"
//#include "rlin21.h"
#include "ostm0.h"
#include "gpio.h"
//#include "rscan.h"
//#include "pwm.h"
//#include "deepstop.h"
#include <stdio.h>
#include <stdlib.h>

/*===========================================================================*/
/* Defines */
/*===========================================================================*/
#define HIGH 1
#define LOW  0

/*===========================================================================*/
/* Prototypes */
/*===========================================================================*/
void              main                ( void );
static void       setup               ( void );
static void       loop                ( void );
__interrupt void  unused_isr          ( void );
unsigned int rndDelay();
static void fillArray();
static void myDelay(int delayTime);
static void digitalWrite(int led, int state);


/*===========================================================================*/
/* Variables */
/*===========================================================================*/
static unsigned int counter = 0;
static unsigned int fastCounter = 0;
static unsigned int *pCounter = NULL;
static unsigned char testArray[10];
static unsigned char testArrayIndex = 0;
static char testArrayData = 'a';
//static unsigned int newDelay;

/*===========================================================================*/
/* Sub Functions */
/*===========================================================================*/
/*****************************************************************************
** Function:    setup
** Description: This function checks the LED, CAN, and LIN functionality and 
**              prints the welcome screen and status via UART
** Parameter:   None
** Return:      None
******************************************************************************/
static void setup(void)
{
  srand(1);
  
  /* Turn LED1 off */
  GPIO_init_output(GPIO_LED1);
  GPIO_output_off(GPIO_LED1);
  
  // initialize the counter pointer
  pCounter = &counter;
}

/*****************************************************************************
** Function:    loop
** Description: This function checks the LED, CAN, and LIN functionality and 
**              prints the welcome screen and status via UART
** Parameter:   None
** Return:      None
******************************************************************************/
static void loop(void)
{
  int newDelay = rndDelay();
  //newDelay = rndDelay();

  digitalWrite(13, HIGH); // turn the LED1 on
  myDelay(newDelay);             // wait for a while
  digitalWrite(13, LOW); // turn the LED off
  myDelay(newDelay);             // wait for a while

  // Increment counter
  counter++;

  if (counter > 100) {
    counter = 0;
  }

  // Fill array
  fillArray();
}

/*===========================================================================*/
/* Interrupt Service Routines - ISR */
/*===========================================================================*/
/*****************************************************************************
** Function:    unused_isr
** Description: Dummy function for unused interrupts
** Parameter:   None
** Return:      None
******************************************************************************/
__interrupt void unused_isr(void)
{
  while(1)
  {
  }
}

/*===========================================================================*/
/* Main function */
/*===========================================================================*/
void main(void) 
{
  /* Clock initialization */
  init_clocks();   /* Set CPU clock to 80 Mhz */
    
  setup();
        
  /* Start the Task-Function */
  //TASKS_start();
  
  while(1) {
      loop();
  }
}

unsigned int __attribute__ ((noinline)) rndDelay() {
    unsigned int delay;
    
    //delay = random (50, 500);
    delay = 50 + (rand() % (500 - 50));
    
    return delay;
}

void __attribute__ ((noinline)) fillArray() {
    // Fill array
    testArray[testArrayIndex] = testArrayData;

    ++testArrayIndex;

    if (testArrayIndex >= 10) {
        testArrayIndex = 0;
    }

    ++testArrayData;

    if (testArrayData > 'z') {
        testArrayData = 'a';
    }
}

void __attribute__ ((noinline)) myDelay(int delayTime) {

    for (int x = 0; x < delayTime; ++x) {
        fastCounter++;
        /* Wait for 1ms */
        OSTM0_compare(40000u);   /* Set compare value to 40000*(1/(CPUCLK/2))=1ms */
        OSTM0_start();
        OSTM0_wait_for_overflow();
    }
}

void digitalWrite(int led, int state) {
  if (state == HIGH) {
    if (led == 13)
      GPIO_output_on(GPIO_LED1);
    return;
  } else {
    if (led == 13)
      GPIO_output_off(GPIO_LED1);
    return;
  }
}