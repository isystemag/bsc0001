/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  pwm.c                                                           */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the PulseWidthModulation Functions                        */
/*                                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "pwm.h"

/*===========================================================================*/
/* Functions */
/*===========================================================================*/
/*****************************************************************************
** Function:    PWMD_init
** Description: Initializes the PWM for the chosen LED
** Parameter:   HPLED1
**              HPLED2
** Return:      None
******************************************************************************/
void PWMD_init (LEDs LED)
{
  /* Configure PWGA output port */
  switch(LED)
  {        
    /* Configure P0_12 as 3rd alternative output PWGA45 */
    case HPLED1:
      /* Set Port 0 Pin 12 to Alt2 output */
      PFC0   |=  (1<<12);
      PFCE0  &= ~(1<<12);
      PFCAE0 &= ~(1<<12);
      PM0    &= ~(1<<12);
      PMC0   |=  (1<<12);
      /* PWM Channel 45 */
      PWGA45CTL = 0x00u;        /* Use PWMCLK0 */
      PWGA45CSDR = 0x00u;       /* Delay = 0 */
      PWGA45CTDR = 375u;        /* Set the trigger for the PWSA to 75us ((1/PWMClock)*PWGA1CTDR) */
      PWGA45CRDR = 0x00u;       /* Duty cycle = 0. This will be updated via duty function */
    break;
        
    /* Configure P0_13 alternative output PWGA46 */
    case HPLED2:
      /* Set Port 0 Pin 13 to Alt2 output */
      PFC0   |=  (1<<13);
      PFCE0  &= ~(1<<13);
      PFCAE0 &= ~(1<<13);
      PM0    &= ~(1<<13);
      PMC0   |=  (1<<13);
      /* PWM Channel 46 */
      PWGA46CTL = 0x00u;        /* Use PWMCLK0 */
      PWGA46CSDR = 0x000u;      /* Delay = 0 */
      PWGA46CTDR = 375u;        /* Set the trigger for the PWSA to 75us ((1/PWMClock)*PWGA2CTDR) */
      PWGA46CRDR = 0x000u;      /* Duty cycle = 0. This will be updated via duty function */
    break;
        
    default:
    break;
  }
}

/*****************************************************************************
** Function:    PWMD_deinit
** Description: Deinitializes the PWM for the chosen LED
** Parameter:   HPLED1
**              HPLED2
** Return:      None
******************************************************************************/
void PWMD_deinit(LEDs LED)
{
  switch(LED)
  {
    /* Reset PWGA1 Port Pins, P10_1 */
    case HPLED1:
      PMC0 &= ~(1u<<12);
      PM0 |= (1u<<12);
      SLPWGA1 &= ~(1u<<13);   /* Stop Channel 1 */
    break;
        
    /* Reset PWGA2 Port Pins, P10_2 */
    case HPLED2:
      PMC0 &= ~(1u<<13);
      PM0 |= (1u<<13);
      SLPWGA1 &= ~(1u<<14);   /* Stop Channel 2 */
    break;
        
    default:
    break;
  }
}

/*****************************************************************************
** Function:    PWMD_start
** Description: Starts the PWM output for the chosen LED
** Parameter:   HPLED1
**              HPLED2
** Return:      None
******************************************************************************/
void PWMD_start(LEDs LED)
{
  switch(LED)
  {
    case HPLED1:
      SLPWGA1 |= 1u<<13;   /* Start Channel 45 */
    break;
        
    case HPLED2:
      SLPWGA1 |= 1u<<14;   /* Start Channel 46 */
    break;
        
    default:
    break;
  }
}

/*****************************************************************************
** Function:    PWMD_stop
** Description: Stops the PWM output for the chosen LED
** Parameter:   HPLED1
**              HPLED2
** Return:      None
******************************************************************************/
void PWMD_stop(LEDs LED)
{
  switch(LED)
  {
    case HPLED1:
      SLPWGA1 &= ~(1u<<13);   /* Stop Channel 45 */
    break;
        
    case HPLED2:
      SLPWGA1 &= ~(1u<<14);   /* Stop Channel 46 */
    break;
        
    default:
    break;
  }
}

/*****************************************************************************
** Function:    PWMDClock_init
** Description: Configure clock for PWM usage
** Parameter:   None
** Return:      None
******************************************************************************/
void PWMDClock_init(void)
{
  PWGA0CTL = 0x00u;   /* Select PWMCLK0 */
  PWBA0BRS0 = 0x04u;  /* Set ClockCycle of PWMCLK0 = PCLK/(2*(PWBA0BRS0[10:0])) = 5MHz */
  PWBA0TS = 0x01u;    /* Starts Output of PWMCLK0 */
}
    
/*****************************************************************************
** Function:    PWMD_duty
** Description: Updates the duty for the chosen LED
**              (In this example set to the potentiometer status read by the ADC)
** Parameter:   LED1
**              LED2
**              HPLED1
**              HPLED2
**
**              duty = 0-4095
** Return:      None
******************************************************************************/
void PWMD_duty(LEDs LED, u16_T duty)
{
  switch(LED)
  {
    case HPLED1:
      PWGA45CRDR = duty;      /* HPLED1 duty cycle = duty/4096 */
      PWGA45RDT = 1u;         /* Load the new duty cycle */
    break;
        
    case HPLED2:
      PWGA46CRDR = duty;      /* HPLED2 duty cycle = ADC/4096 */
      PWGA46RDT = 1u;         /* Load the new duty cycle */
    break;
        
    default:
    break;
  }
}
    
/*****************************************************************************
** Function:    PWSA_init
** Description: Initialize the PWM-Diagnostic function
** Parameter:   None
** Return:      None
******************************************************************************/    
void PWSA_init(void)
{
    PWSA0PVCR44_45 = 0x00810000u;   /* [31:16]PWSA virtual channel 01: [23:22] check ADCA0ULLMTBR1 / [21:16] channel link to PWGADIN1 */
    PWSA0PVCR46_47 = 0x000000c2u;   /* [15:0] PWSA virtual channel 02: [23:22] check ADCA0ULLMTBR2 / [21:16] channel link to PWGADIN2 */
}

/*****************************************************************************
** Function:    PWSA_start
** Description: Starts the PWM-Diagnostic function
** Parameter:   None
** Return:      None
******************************************************************************/    
void PWSA_start(void)
{
    ADCA0PWDSGCR = 1u;      /* PVCR Trigger enabled */
    PWSA0CTL = 1u;          /* Start PWSA */
}

/*****************************************************************************
** Function:    PWSA_stop
** Description: Stops the PWM-Diagnostic function
** Parameter:   None
** Return:      None
******************************************************************************/    
void PWSA_stop(void)
{
    ADCA0PWDSGCR = 0u;      /* PVCR Trigger disabled */
    PWSA0CTL = 0u;          /* Stop PWSA */
}