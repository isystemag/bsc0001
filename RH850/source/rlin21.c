/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  rlin21.c                                                        */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the LIN Functions                                         */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "rlin21.h"

/*===========================================================================*/
/* Functions */
/*===========================================================================*/

/*****************************************************************************
** Function:    RLIN21_Test
** Description: Checks the LIN-Macro function by sending and verifying data
** Parameter:   None
** Return:      no error    0
**              error       ErrorStatusRegister
******************************************************************************/
u32_T RLIN21_Test(void)
{
  u32_T return_value = 0u;
  /* Reset is caused */ 
  do{
      RLN2401L1CUC = 0x00u;
  }while(RLN2401L1MST !=0u );
  
    /* LIN Mode Register */
  RLN2401L1MD = 0x00u;      /* LIN System Clock: fa */
  
  /* LIN Break Field Configuration Register */
  RLN2401L1BFC = 0x15u;     /* b3-b0=0101: break width 18Tbits; b5b4=01: break delimiter 2Tbit */
  
  /* LIN Wake-up Baud Rate Selector register  */
  RLN240GLWBR = 0X01u;
  
  RLN2401L1SC = 0x17u;     /* Inter-byte(header/response) 7Tbits and interbyte 1Tbit */

  RLN2401L1WUP=0x30u;      /* b7-b4=0100: Wake-up Transmission low width 4 bits */
   
  /* LIN Baud Rate Prescaler1 */
  RLN240GLBRP1 = 0x02;     /* Baudrate = PCLK / (((BRP value) + 1)*(number of samples per bit)) */
  RLN240GLBRP0 = 0x81;     /* 40MHZ/((129+1)*16)=19200 */

  RLN2401L1IE = 0x03u;       /* Tx/Rx interrupt enabled */
  

  
  /* LIN Error Detection Enable Register */
  RLN2401L1EDE = 0x0fu;      /* Enable error detection */

  
  RLN2401L1IDB = 0x08u;      /* LIN ID Buffer Register ID and parity settings */
  
  /* Fill LIN data buffer register */
  RLN2401L1DBR1 = 0x55u;    
  RLN2401L1DBR2 = 0x55u;    
  RLN2401L1DBR3 = 0x55u;
  RLN2401L1DBR4 = 0x55u;
  RLN2401L1DBR5 = 0x55u;
  RLN2401L1DBR6 = 0x55u;
  RLN2401L1DBR7 = 0x55u;
  RLN2401L1DBR8 = 0x55u;  
  
  RLN2401L1DFC = 0x28u;     /* Enhanced checksum, response field length 8byte + checksum */
  
  /* Reset is cancelled / operating mode is caused */ 
  do
  {
    RLN2401L1CUC = 0x03u;
  }while(RLN2401L1MST != 0x03u);
  
  RLN2401L1DFC |= 1u<<4;    /* Set mode to transmission */
  RLN2401L1TRC |= 0x01u;    /* Set start bit */
  
  while(RLN2401L1ST!=0x81u)  /* Wait while frame transmission not completed */
  {    
    if(RLN2401L1ST==8u)     /* If error occurred */
    {        
      return_value = (u32_T)RLN2401L1EST;   /* Return error */
      break;
    }    
  }

  return return_value;
}

/*****************************************************************************
** Function:    RLIN21_port_init
** Description: Configures P0_8 as alternative function RLIN21TX 
**              Configures P0_7 as alternative function RLIN21RX 
** Parameter:   None
** Return:      None
******************************************************************************/
void RLIN21_port_init(void)
{
  /* Configure RLIN21 Port Pins */ 
  /* Configure P0_8 as 1st alternative output RLIN21TX */
  PMC0  |= 1u << 8;
  PFCE0 &= ~(1u << 8);
  PFC0  &= ~(1u << 8);
  PFCAE0&= ~(1u << 8);
  PM0   &= ~(1u << 8);
    
  /* Configure P0_7 as 1st alternative input RLIN21RX */
  PMC0  |= 1u << 7;
  PFCE0 &= ~(1u << 7);
  PFC0  &=  ~(1u << 7);
  PFCAE0&= ~(1u << 7);
  PM0   |= 1u << 7;
}