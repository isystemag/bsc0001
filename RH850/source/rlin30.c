/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  rlin30.c                                                        */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the UART Functions                                        */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "rlin30.h"

/*****************************************************************************
** Function:    RLIN30_init
** Description: Initialize the RLIN30 for UART 
** Parameter:   None
** Return:      None
******************************************************************************/
void RLIN30_init(void)
{
  /* RLIN30 is configured in UART mode with 9600 baud */
  /* Disable RLIN */ 
  RLN30LUOER=0x00u;
  RLN30LCUC=0x00u;

  /* LIN Mode Register/UART Mode Register (LMD) */
  RLN30LMD=0x01u;      /* UART mode */
                       /* LIN / UART System Clock: fa */
                       /* Module generates 1 interrupt signal */
                       /* 3-bit majority voting logic for sampling RX data is enabled */
                
  /* LIN Break Field Configuration Register/UART Configuration Register1 */
  RLN30LBFC=0x00u;     /* UART 8-bit communication */
                       /* LSB first */
                       /* Stop Bit 1 bit */
                       /* Parity Disabled */
                       /* Without RX inversion */
                       /* Without RX inversion */
  
  /* LIN / UART Error Detection Enable Register */
  RLN30LEDE=0x00u;     /* No error detection */

  /* LIN Wake-up Baud Rate Selector register  */
  RLN30LWBR=0x51u;     /* 6 samples per bit */
                  
  
  /* LIN Baud Rate Prescaler1/UART Baud Rate Prescaler */
  RLN30LBRP1=0x02u;    /* Baudrate = PCLK / (((BRP value) + 1)*(number of samples per bit)) */
  RLN30LBRP0=0xb5u;    /* 40MHZ/((0x2b5+1)*6)= 9600 baud */


  /* LIN / UART Control Register */
  RLN30LCUC=0x01u;     /* Set SW Reset request to inactive */


  /* UART Operation Enable Register */ 
  RLN30LUOER=0x03u;    /* UART Transmission Operation Enable Bit */
                       /* UART Reception Operation Enable Bit */
}

/*****************************************************************************
** Function:    RLIN30_port_init
** Description: Configures P0_2 to alternative function RLIN30TX
**              Configures P0_3 to alternative function RLIN30RX
** Parameter:   None
** Return:      None
******************************************************************************/
void RLIN30_port_init(void)
{
  /* Configure RLIN30 Port Pins */ 
  /* RLIN30 TX on PIN 2 of Port 0 */
  PMC0  |= 1u << 2;
  PFCE0 &= ~(1u << 2);
  PFC0  |= (1u << 2);
  PFCAE0&= ~(1u << 2);
  PM0   &= ~(1u << 2);
    
  /* RLIN30 RX on PIN 3 of Port 0 */
  PMC0  |= 1u << 3;
  PFCE0 &= ~(1u << 3);
  PFC0  |=  1u << 3;
  PFCAE0&= ~(1u << 3);
  PM0   |= 1u << 3;
}


/*****************************************************************************
** Function:    RLIN30_send_string
** Description: Sends out a complete string via UART by using DirectMemoryAccess
** Parameter:   string to be send
** Return:      None
******************************************************************************/
void RLIN30_send_string( u08_T send_string[] )
{
  u16_T  us_string_length= 0x00u;

  /* Calculate length of string send_string [] */
  while( send_string[ us_string_length ] != (u08_T)'\0' )
  {
    ++us_string_length;
  }
  
  while((RLN30LST&16u)==16u)
  {
    /* Wait until RLIN transmission is finished */
  }
   
  while(DTS0DTE != 0u)
  {
    /* Wait until last transmission is finished */
  } 
   
   /* Initialize DMA for sending bytes */
  DTS0 = 0x00u;                      /* Disable DMA */
  DTC0 = us_string_length-1u;        /* Number of times the DMA transfers(string length -1 because first trigger is manually given) */
  #if COMPILER == COMP_IAR
    #pragma diag_suppress=Pm140
  #endif
  DSA0 = (u32_T)&send_string[1u];    /* Source Address = string buffer */
  DDA0 = (u32_T)&RLN30LUTDR;         /* Destination Address = RLIN transmit register */
  #if COMPILER == COMP_IAR
    #pragma diag_default=Pm140
  #endif
  DTCT0 = 0x0020u;                   /* 8bit TransferSize / No Loop / Fixed Destination / Increment Source */
  DTFR0 = 0x800au;                   /* Trigger Enabled / Source = RLIN30 transmit interrupt */
  DRQCLR = 1u;                       /* Clear DMA channel 0 request */
  DTS0 |= 0x01u;                     /* Enable DMA channel 0 transfer */
  /* Start transmission */
  RLN30LUTDR = (u08_T) send_string[0]; /* Write first char to RLIN transmit register to start transmission */
}

/*****************************************************************************
** Function:    RLIN30_get_status
** Description: Returns if RLIN30 is busy or ready
** Parameter:   None
** Return:      RLIN30_busy
**              RLIN30_ready
******************************************************************************/
enum RLIN30_status RLIN30_get_status(void)
{
  enum RLIN30_status return_value;
  if((RLN30LST&16u)==16u)
  {
    return_value = RLIN30_busy;    /* Return RLIN30 is busy */
  }
  else                             /* If transmission bit is 0 (transmission ready) */
  {
    return_value = RLIN30_ready;   /* Return RLIN30 is ready */
  }
  return return_value;
}
