/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  rscan.c                                                         */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the RS CAN Functions                                      */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "rscan.h"
#include "can_table.h"

/*===========================================================================*/
/* Functions */
/*===========================================================================*/
/*****************************************************************************
** Function:    RS_CAN_init
** Description: Configures the CAN0/CAN1 macros
** Parameter:   None
** Return:      None
******************************************************************************/
void RS_CAN_init(void)
{  
  /* Configure CAN0 GPIO-EnablePin P1_1 */
  PMC1 &= ~(1u<<1);
  PM1 &= ~(1u<<1);
  P1 &= ~(1u<<1);        /* First disabled */
    
  /* Configure CAN1 GPIO-EnablePin P2_6 */ 
  PMC2 &= ~(1u<<6);
  PM2 &= ~(1u<<6);
  P2 &= ~(1u<<6);        /* First disabled */
    
  /* Set CAN3TX as P1_3 and CAN3RX as P1_2 */
  PMC1 |= 0x000cu;                                                      
  PFC1 &= ~(0x000cu);                                                      
  PM1 &= ~(1u<<3);                                                       
  PM1 |= 1u<<2;                                                       

  /* Set CAN4TX as P1_13 and CAN4RX as P1_12 */                       
  PMC1 |= 0x3000u;                                                      
  PFC1 &= ~(0x3000u);                                                      
  PM1 &= ~(1u<<13);                                                       
  PM1 |= 1u<<12;  
        
  /* Wait while CAN RAM initialization is ongoing */
  while((RSCAN0GSTS & 0x00000008u)) {}

  /* Switch to global/channel reset mode */
  RSCAN0GCTR &= 0xfffffffbu;
  RSCAN0C3CTR &= 0xfffffffbu;
  RSCAN0C4CTR &= 0xfffffffbu;

  /* Configure clk_xincan as CAN-ClockSource */
  RSCAN0GCFG = 0x00000010u;
    
  /* When fCAN is 16MHz, 
  Bitrate = fCAN/(BRP+1)/(1+TSEG1+TSEG2) = 16/2/16 = 0.5Mbps(500Kbps) */

  RSCAN0C3CFG = 0x023a0001u;    /* SJW =3TQ, TSEG1=10TQ, TSEG2=4TQ, BRP=1 */

  RSCAN0C4CFG = 0x023a0001u;    /* SJW =3TQ, TSEG1=10TQ, TSEG2=4TQ, BRP=1 */

  /* ==== Rx rule setting ==== */
  Can_SetRxRule();

  /* ==== Buffer setting ==== */    
  RSCAN0RMNB = 0x10u;  /*Can_SetGlobalBuffer */
    
  /* Set THLEIE disabled, MEIE(FIFO Message Lost Interrupt disabled) */
  RSCAN0GCTR &= 0xfffff8ffu;    

  /* If GlobalChannel in halt or reset mode */
  if (RSCAN0GSTS & 0x03u) 
  {
    RSCAN0GCTR &= 0xfffffffcu;     /* Switch to communication mode */
    while ((RSCAN0GSTS & 0x02u) == 2u)
	{
      /* While halt mode */
    }
    while ((RSCAN0GSTS & 0x01u) == 1u) 
	{
      /* While reset mode */
    }
  }

  /* If Channel3 in halt or reset mode */
  if (RSCAN0C3STS & 0x03u) 
  {
    RSCAN0C3CTR &= 0xfffffffcu;    /* Switch to communication mode */
    while ((RSCAN0C3STS & 0x02u) == 2u) 
	{
      /* While halt mode */
    }
    while ((RSCAN0C3STS & 0x01u) == 1u) 
	{
      /* While reset mode */
    }
  }
    
  /* Enable CAN0 by setting P1_1 high */
  P1 |= 1u<<1;    
     
  /* If Channel3 in halt or reset mode */
  if (RSCAN0C4STS & 0x03u) 
  {
    RSCAN0C4CTR &= 0xfffffffcu;    /* Switch to communication mode */
    while ((RSCAN0C4STS & 0x02u) == 2u) 
	{
      /* While halt mode */
    }
    while ((RSCAN0C4STS & 0x01u) == 1u) 
	{
     /* While reset mode */
    }
  }
  /* Enable CAN1 by setting P2_6 high */
  P2 |= 1u<<6;    
}

/*****************************************************************************
** Function:    Can_SetRxRule
** Description: Set all Rx rules
** Parameter:   None
** Return:      None
******************************************************************************/
static void Can_SetRxRule(void)
{
  u16_T RxRuleIdx;
  u08_T PageRxRuleIdx;
  volatile CAN_RX_RULE_TYPE* pCRE;

  /* Set Rx rule number per channel */
  RSCAN0GAFLCFG0 |= 0x00000006u;    /* Channel3 rule number is 6 */

  RSCAN0GAFLCFG1 |= 0x06000000u;    /* Channel4 rule number is 6 */

  /* Get access base address of Rx rule */
  pCRE = (volatile CAN_RX_RULE_TYPE*)&(RSCAN0GAFLID0);

  /* Receive Rule Table Write Enable */
  RSCAN0GAFLECTR |= 0x00000100u;

  /* Copy Rx rule one by one */
  for (RxRuleIdx = 0U; RxRuleIdx < CAN_RX_RULE_NUM; RxRuleIdx++) 
  {
    PageRxRuleIdx = (u08_T) (RxRuleIdx & CAN_PAGE_RX_RULE_IDX_MASK);

    /* Update target Rx rule page if necessary */
    if (PageRxRuleIdx == 0U) 
    {
      RSCAN0GAFLECTR |= RxRuleIdx >> CAN_RX_RULE_PAGE_IDX_BIT_POS;
    }

    /* Set a single Rx rule.*/
    pCRE[PageRxRuleIdx] = CAN_RX_RULE_TABLE[RxRuleIdx];
  }

  /* Rx rule write disable */
  RSCAN0GAFLECTR &= 0xfffffeffu;
}

/*****************************************************************************
** Function:    Can_ReadRx_buffer
** Description: This code shows how to read message from Rx buffer
** Parameter:   pRxBufIdx - Pointer to Rx buffer that receives frame
** Return:      CAN_RTN_OK           - A frame is successfully read out
**              CAN_RTN_BUFFER_EMPTY - No frame is read out   
******************************************************************************/
Can_RtnType Can_ReadRxBuffer(Can_FrameType* pFrame)
{
  u08_T BufIdx;
  u08_T CRBRCFiBufIdx;
  u08_T OverwrittenFlag;
  u32_T TempCRBRCF0;
  u32_T TempCRBRCF1;
  u32_T TempCRBRCF2;
  Can_FrameType* pRxBuffer;
  vu32_T* pCRBRCF;
  Can_RtnType RtnValue;

  /* Judge if new messages are available */
  TempCRBRCF0 = RSCAN0RMND0;
  TempCRBRCF1 = RSCAN0RMND1;
  TempCRBRCF2 = RSCAN0RMND2;
  if ((TempCRBRCF0 == CAN_CLR) && (TempCRBRCF1 == CAN_CLR)&& (TempCRBRCF2 == CAN_CLR)) 
  {
    RtnValue = CAN_RTN_BUFFER_EMPTY;
  }
  else
  {
    /* Get Rx buffer that has new message */
    if (TempCRBRCF0 != CAN_CLR) 
    {
      pCRBRCF = &(RSCAN0RMND0);
      for (BufIdx = 0U; BufIdx < CAN_CRBRCF0_RX_BUF_NUM; ++BufIdx) 
      {
        if ((TempCRBRCF0 & CAN_1_BIT_MASK) == CAN_SET) 
        {
          break;
        }
        TempCRBRCF0 = TempCRBRCF0 >> CAN_B1_BIT_POS;
      }
    }
    else if (TempCRBRCF1 != CAN_CLR)
    {
      pCRBRCF = &(RSCAN0RMND1);
      for (BufIdx = 0U; BufIdx < CAN_CRBRCF1_RX_BUF_NUM; ++BufIdx) 
      {
        if ((TempCRBRCF1 & CAN_1_BIT_MASK) == CAN_SET) 
        {
          break;
        }
        TempCRBRCF1 = TempCRBRCF1 >> CAN_B1_BIT_POS;
      }
      BufIdx += CAN_CRBRCF0_RX_BUF_NUM;
    }
    else if (TempCRBRCF2 != CAN_CLR)
    {
      pCRBRCF = &(RSCAN0RMND2);
      for (BufIdx = 0U; BufIdx < CAN_CRBRCF2_RX_BUF_NUM; ++BufIdx) 
	  {
        if ((TempCRBRCF2 & CAN_1_BIT_MASK) == CAN_SET) 
	    {
          break;
        }
        TempCRBRCF2 = TempCRBRCF2 >> CAN_B1_BIT_POS;
      }
      BufIdx += (CAN_CRBRCF0_RX_BUF_NUM+CAN_CRBRCF1_RX_BUF_NUM);
    }
    /* Calculate index value in CRBRCFi */
    CRBRCFiBufIdx = BufIdx & CAN_5_BIT_MASK;

    do 
    {
      /* Clear Rx complete flag of corresponding Rx buffer */
      do 
        {
          CLR_BIT(*pCRBRCF, CRBRCFiBufIdx);
        }while (GET_BIT(*pCRBRCF, CRBRCFiBufIdx) == CAN_SET);

        /* Read out message from Rx buffer */
        pRxBuffer = (Can_FrameType*) &(RSCAN0RMID0);
        *pFrame = pRxBuffer[BufIdx];

        /* Judge if current message is overwritten */
        OverwrittenFlag = GET_BIT(*pCRBRCF, CRBRCFiBufIdx);
        /* Message is overwritten */
        if (OverwrittenFlag == CAN_SET) 
        {
          /* User process for message overwritten */
        }
    }while (OverwrittenFlag == CAN_SET);

    RtnValue = CAN_RTN_OK;
  }
  return RtnValue;
}


Can_RtnType Can_C3TrmByTxBuf(u08_T TxBufIdx, const Can_FrameType* pFrame)
{
  vu08_T* pTBSR;
  Can_FrameType* pTxBuffer;
  vu08_T* pTBCR;

  pTBSR = &(RSCAN0TMSTS48);
  pTBCR = &(RSCAN0TMC48);

  /* ---- Return if Tx Buffer is transmitting. ---- */    
  if( (pTBSR[TxBufIdx] & (vu08_T)0x01u) == CAN_TBTST_TRANSMITTING )
  {
    return CAN_RTN_ERR;
  }

  /* Clear Tx buffer status */
  do 
  {
    pTBSR[TxBufIdx] = CAN_CLR;
  }while (pTBSR[TxBufIdx] != CAN_CLR);

  /* Store message to Tx buffer */
  pTxBuffer = (Can_FrameType*) &(RSCAN0TMID48);
  pTxBuffer[TxBufIdx] = *pFrame;

  /* Set transmission request */
  pTBCR[TxBufIdx] = CAN_TBCR_TRM;

  return CAN_RTN_OK;
}


Can_RtnType Can_C4TrmByTxBuf(u08_T TxBufIdx, const Can_FrameType* pFrame)
{
  vu08_T* pTBSR;
  Can_FrameType* pTxBuffer;
  vu08_T* pTBCR;

  pTBSR = &(RSCAN0TMSTS64);
  pTBCR = &(RSCAN0TMC64);

  /* ---- Return if Tx Buffer is transmitting. ---- */
  if( (pTBSR[TxBufIdx] & (vu08_T)0x01u) == CAN_TBTST_TRANSMITTING )
  {
    return CAN_RTN_ERR;
  }

  /* Clear Tx buffer status */
  do 
  {
    pTBSR[TxBufIdx] = CAN_CLR;
  }while (pTBSR[TxBufIdx] != CAN_CLR);

  /* Store message to Tx buffer */
  pTxBuffer = (Can_FrameType*) &(RSCAN0TMID64);
  pTxBuffer[TxBufIdx] = *pFrame;

  /* Set transmission request */
  pTBCR[TxBufIdx] = CAN_TBCR_TRM;

  return CAN_RTN_OK;
}

/* Create Can_FrameType for send and receive data */
const Can_FrameType CANTraStandData={
  /*CiTBpA */
  0x18u,
  0u,
  0u,
  0u,        
    
  /*CiTBpB */
  0x0000u,                            
  0x000u,                            
  0x8u,    
    
  {
    0x12u,                            /*DB0 */
    0x34u,                            /*DB1 */
    0x56u,                            /*DB2 */
    0x78u,                            /*DB3 */
    /*CiTBpD */
    0x87u,                            /*DB4 */
    0x65u,                            /*DB5 */
    0x43u,                            /*DB6 */
    0x21u                             /*DB7 */
  }
};

Can_FrameType CANRecData={
  /*CiTBpA */
  0x00u,
  0u,
  0u,
  0u,
    
  /*CiTBpB */
  0x0000u,                                
  0x000u,                            
  0x0u,                        

  /*CiTBpC */
  {
    0x00u,                            /*DB0 */
    0x00u,                            /*DB1 */
    0x00u,                            /*DB2 */
    0x00u,                            /*DB3 */
    /*CiTBpD */
    0x00u,                            /*DB4 */
    0x00u,                            /*DB5 */
    0x00u,                            /*DB6 */
    0x00u                             /*DB7 */
  }
};


 /*****************************************************************************
** Function:    RS_CAN_error
** Description: This function sends/receives and compares data of the CAN-Channels
** Parameter:   None
** Return:      error 1
**              no error 0  
******************************************************************************/
u32_T RS_CAN_error(void)
{
  u32_T rs_count, error,i;

  if(Can_C3TrmByTxBuf(1u,&CANTraStandData)== CAN_RTN_OK)
  {
    /*Delay */
    for(i=0;i<100000;i++)
	{
	  /* Wait for CAN receive interrupt */
	}                
    Can_ReadRxBuffer(&CANRecData);  /* Channel4 receive the Messages */
  }
    
  /* Compare each sent and received value */
  error=0;
  for(rs_count=0; rs_count<8; rs_count++)
  {
    if(CANTraStandData.DB[rs_count] != CANRecData.DB[rs_count])
    {
      error=1;
    }
  }
  return error;
}
