/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  tasks.c                                                         */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the task handling                                         */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "button.h"
#include "ostm0.h"
#include "pwm.h"
#include "adc.h"
#include "gpio.h"
#include "deepstop.h"
#include "tasks.h"
#include "tau.h"
#include "lps.h"
#include "clocks.h"
#include "rlin30.h"
#include <stdio.h>

/*===========================================================================*/
/* Defines */
/*===========================================================================*/
#define HP_LED_DIMM_FACTOR        4u
#define RANGE_CHECK_VALUE         40
#define STATUS_UPDATE_INTERVAL    200u
#define DEEPSTOP_COUNT_DOWN       30000u
#define DEEPSTOP_POTI_SENSE       512u

/*===========================================================================*/
/* Prototypes */
/*===========================================================================*/
void mode_1(void);
void mode_2(void);

/*===========================================================================*/
/* Variables */
/*===========================================================================*/
enum direction g_mode1_direction;
u32_T g_mode1_duty;

/* Mode is stored in Retention RAM (so it is retained during DEEPSTOP) */
#if COMPILER == COMP_GHS
  #pragma ghs startdata 
  #pragma ghs section bss  = ".rdata"
  SYSTEM_Status_S g_System_Status;
  #pragma ghs section bss = default
  #pragma ghs enddata
#endif

#if COMPILER == COMP_IAR
  #pragma location="RETENTION_RAM.noinit"
  __no_init __brel23 SYSTEM_Status_S g_System_Status;
#endif

/* Text array for print function with a maximum length of 100 */
u08_T g_status_print[100];

/* Error counter for controlled error recognition of HPLED feddback signals */
u08_T g_HPLED1_error_counter = 0,
      g_HPLED2_error_counter = 0;

/* storage for  ADC values*/
u16_T g_adc0_value=0u;
u16_T g_adc1_value=0u;

/* Variables for the error status and error send status of the both HighPowerLEDs */
enum error g_HPLED1_error = not_occurred, g_HPLED2_error = not_occurred;
enum send g_HPLED1_error_send = not_sent, g_HPLED2_error_send = not_sent;

/*===========================================================================*/
/* Functions */
/*===========================================================================*/
/*****************************************************************************
** Function:    print_status
** Description: This function sends the status of POT1/POT2 and mode time 
**              via UART
** Parameter:   None
** Return:      None
******************************************************************************/
void print_status(void)
{   
  u08_T * tmp = g_status_print;
  sprintf((char*)tmp, "\rPOT1:%3i%% POT2:%3i%%  DeepStop in %2is \r", (u16_T)(((g_adc0_value+35u)*100u)/4095u), (u16_T)(((g_adc1_value+20u)*100u)/4095u), (u32_T)(30u-(g_System_Status.deepstop_counter/1000u)));
    
  /* Send Message via RS323 */
  RLIN30_send_string(g_status_print);
}

/*****************************************************************************
** Function:    print_text
** Description: This function sends the message to the compiler I/O and via UART
** Parameter:   text_array[] - Text to be sent
** Return:      None
******************************************************************************/
void print_text(u08_T* text_array)
{
  /* Send Message via Debugger Output */
  printf((const char*)text_array);
  /* Send Message via RS323 */
  RLIN30_send_string(text_array);
}

/*****************************************************************************
** Function:    range_check
** Description: Checks if a value is within a certain range
** Parameter:   text_array[] - Text to be sent
** Return:      0 - value is within range
**              1 - value is out of range 
******************************************************************************/
u08_T range_check(s32_T ref_value, s32_T new_value)
{
  u08_T return_value = 0u;
  if((((s32_T)ref_value-RANGE_CHECK_VALUE)> new_value)||(((s32_T)ref_value+RANGE_CHECK_VALUE)<new_value))
  {
    return_value = 1u;          /* Value is out of range */
  }
  return return_value;          /* Value is within range */
}

/*****************************************************************************
** Function:    ADCA0_error_cb
** Description: This function is called when an ADCA0 error interrupt is
**              generated (PWSA limit or potentiometer action)
**              It turns off the PWM of the HPLED, which generated an 
**              Upper-/Lower Limit Interrupt or resets the System timer
** Parameter:   Upper/Lower limit info
** Return:      None
******************************************************************************/
void ADCA0_error_cb(u32_T  ULER)
{
  switch(ULER&3u)
  {
    case 1u:
    	g_HPLED1_error_counter++;
    	if(g_HPLED1_error_counter >=3)
    		{
    			PWMD_stop(HPLED1);                          /* Stop PWM */
         if(g_HPLED1_error_send == not_sent)                        /* If error for HPLED1 is not already sent */
         {
           print_text("\n\rHPLED1 Error - Stop HPLED1PWM\n\r");     /* ErrorOut via UART */
           g_HPLED1_error_send = sent;
         }
         g_HPLED1_error = occurred;                                 /* Error status: error is occurred for HPLED1 */
        }
    break;
	
    case 2u:
    	g_HPLED2_error_counter++;
    	if(g_HPLED2_error_counter >=3)
    		{
         PWMD_stop(HPLED2);                                         /* Stop PWM */
         if(g_HPLED2_error_send == not_sent)                        /* If error for HPLED2 is not already sent */
           {
             print_text("\n\rHPLED2 Error - Stop HPLED2PWM\n\r");   /* ErrorOut via UART */
             g_HPLED2_error_send = sent;
           }
         g_HPLED2_error = occurred;                                 /* Error status: error is occurred for HPLED2 */
         
        }
    break;
        
    default:
    break;
  }
}


/*****************************************************************************
** Function:    PWM_TimerInt
** Description: This function is called when the TAUJ generates an the 
**              interrupt in mode 1
**              It creates the alternating signal of the (HP)LEDs depending 
**              on the potentiometer position
** Parameter:   None
** Return:      None
******************************************************************************/
void PWM_TimerInt(void)
{
  /* Potentiometer 1 sets the TAUJ-Interrupt Cycle */
  TAUJ_dr_update((u16_T)1u, ((u32_T)((g_adc0_value*0x10)+0x100)));
    
  /* Update the LED duties in dependence to temp value */
  TAUJ_dr_update((u16_T)3u, (u32_T)((g_mode1_duty*g_adc1_value)/0x1000u));
  TAUB_dr_update((u16_T)8u, (u32_T)(g_adc1_value-(g_mode1_duty*g_adc1_value)/0x1000u)); 
  PWMD_duty(HPLED1, (u16_T) ((g_mode1_duty*g_adc1_value)/0x1000u)/HP_LED_DIMM_FACTOR); 
  PWMD_duty(HPLED2, (u16_T) (g_adc1_value-(g_mode1_duty*g_adc1_value)/0x1000u)/HP_LED_DIMM_FACTOR); 
    
  /* Temp counts up/down for alternating LED-Signal */
  if(g_mode1_direction == up)
  {
    g_mode1_duty += 100u;
    if(g_mode1_duty >= 0xFA0u)
      {
        g_mode1_direction = down;   /* When maximum value is reached change count direction downwards */
      }
  }
  if(g_mode1_direction == down)
  {
    g_mode1_duty -= 100u;
    if(g_mode1_duty == 0u)
    {
      g_mode1_direction = up;       /* When minimum value is reached change count direction upwards */
    }
  }
}

/*****************************************************************************
** Function:    mode_1
** Description: The first mode you can run
**              In this mode the alternating LED-signals are generated with the
**              PWM_TimerInt call back function of the TAUJ channel 1 interrupt
** Parameter:   None
** Return:      None
******************************************************************************/
void mode_1(void)
{
  u16_T adc0_temp;
  u16_T adc1_temp;
  u08_T loop_mode=1u;
  u08_T range1;
  u08_T range2;
  enum button_states temp_buttom_state;
  /* Initialize the APO Pin for the ADC usage */
  GPIO_init_output(GPIO_APO);
  GPIO_output_on(GPIO_APO);
      
  /* LED1 / LED2 TAU INIT */
  TAUJ_LED2_start();
  TAUB_LED1_start();
        
  /* HPLED1 / HPLED2 PWMD INIT */
  PWMD_init(HPLED1);
  PWMD_init(HPLED2);
  PWMD_start(HPLED1);
  PWMD_start(HPLED2);
 
  /* Initialize the PWM diagnostic function */
  PWSA_init();                         /* PWSA for HPLED-Check */
  ADCA0_limit(1u, 0x19Au, 0x094u);     /* Set limits(limit channel 1) for PWSA */
  ADCA0_limit(2u, 0x19Au, 0x094u);     /* Set limits(limit channel 2) for PWSA  */
  PWSA_start();                        /* Start the PWSA */
     
  /* Set default value for LED PWM duty cycle */ 
  g_mode1_duty=2000u;
    
  /* Start interval timer for PWM update in mode1 */
  TAUJ_INT_start();             

  g_System_Status.ticks = 0u;                /* Resets the counter value System_Status.ticks */
  g_System_Status.deepstop_counter = 0u;     /* Resets the counter value System_Status.deepstop_counter */
   
  while(loop_mode)
  {
    /* Read current potentiometer values */
    g_adc0_value=ADC_read(0u);
    g_adc1_value=ADC_read(1u);     

    /* Check if any button was pressed */
    button_handler();                                  /* Call ButtonHandler */
    temp_buttom_state=button_P0_9_status();            /* save button status to temp variable */
     
    if(temp_buttom_state == button_state_short)        /* Check if button was pressed shortly */
    { 
      g_System_Status.ticks = 0u;                      /* Reset 30sTimer when Button is pressed */
      print_text("\n\n\r>>Switch to Mode 2\n\r");      /* Send ModeSwitch via UART */
      g_System_Status.current_mode = mode2;            /* Mode is now mode 2 */
      loop_mode=0u; 
    }              
    else if(temp_buttom_state == button_state_long)                       /* Enter DeepStop after button is pressed for 3s or longer */
    {                    
      print_text("\n\n\r>>Button pressed 3s - enter DeepStop\n\r");    
      while(RLIN30_get_status() == RLIN30_busy){}
      g_System_Status.current_mode = deepstop;                            /* Mode is now DEEPSTOP */
      loop_mode=0u;
    } 
    else if(g_System_Status.deepstop_counter >= DEEPSTOP_COUNT_DOWN)      /* Enter DeepStop after 30s */
    {                                        
      print_text("\n\n\r>>30s without action - enter DeepStop\n\r");
      while(RLIN30_get_status() == RLIN30_busy){}
      g_System_Status.current_mode = deepstop;                            /* Mode is now DEEPSTOP */
      loop_mode=0u;
    }
      
    /* HPLED error check after 10ms */
    if((g_HPLED1_error == not_occurred) && (g_System_Status.ticks == 10u))   /* If no error for HPLED1 occurs after 10ms */
    {
      g_HPLED1_error_send = not_sent;                                        /* Reset the send status of HPLED1 */
    }
    if((g_HPLED2_error == not_occurred) && (g_System_Status.ticks == 10u))   /* If no error for HPLED2 occurs after 10ms */
    {
      g_HPLED2_error_send = not_sent;                                        /* Reset the send status of HPLED2 */
    }
    
    /* Error counters are decremented every 30ms. Thus an error has to occur 3 times in a row to execute the error handling. */
    if((g_HPLED1_error_counter > 0) && !(g_System_Status.ticks % 30))
    	{
    		g_HPLED1_error_counter--;
    	}
    	
    if((g_HPLED2_error_counter > 0) && !(g_System_Status.ticks % 30))
    	{
    		g_HPLED2_error_counter--;
    	}

    if(!(g_System_Status.ticks % STATUS_UPDATE_INTERVAL))
    {
      /* Check if the potentiometer values have changed and reset DeepStop timer counter if necessary */
      range1 = range_check((s32_T)adc0_temp, (s32_T)g_adc0_value);
      range2 = range_check((s32_T)adc1_temp, (s32_T)g_adc1_value);
      if( (range1 || range2) == 1 )
      { 
        g_System_Status.deepstop_counter=0u;
      }
  
      /* Write current ADC values to temp variable */
      adc0_temp=g_adc0_value;
      adc1_temp=g_adc1_value;
  
      if(RLIN30_get_status() == RLIN30_ready)
      {    
        print_status();
      }
    }
    
    OSTM0_wait_for_overflow();               /* Wait for timer tick */
    g_System_Status.ticks++;                 /* Increment global counter */
    g_System_Status.deepstop_counter++;  
  }
  /* Leave mode1 */
  g_System_Status.previous_mode = mode1;     /* Previous mode is mode 1 */
    
  /* Stop all used functions */
  TAUJ_INT_stop();                            /* Stop TAU-Interrupt */
      
  PWSA_stop();                               /* Stop PWSA */
    
  /* Stop all PWMs of the LEDs */
  TAUJ_LED2_stop();
  TAUB_LED1_stop();
  PWMD_deinit(HPLED1);
  PWMD_deinit(HPLED2);
  
  /* Reset the error status for both HPLEDs */
  g_HPLED1_error = not_occurred;
  g_HPLED1_error_counter = 0;
  g_HPLED2_error = not_occurred;
  g_HPLED2_error_counter = 0;

  GPIO_deinit(GPIO_APO);               /* Deinitialize the APO Pin used for the ADC macro */
}

/*****************************************************************************
** Function:    mode2
** Description: The second mode you can run
**              In this mode you can vary the intensity of the LEDs by using
**              the potentiometers
** Parameter:   None
** Return:      None
******************************************************************************/
void mode_2(void)
{
  enum button_states temp_buttom_state;
  u16_T adc0_temp;
  u16_T adc1_temp;
  u08_T loop_mode=1u;
    
  u08_T range1;
  u08_T range2;
    
  /* Initialize the APO Pin for the ADC usage */
  GPIO_init_output(GPIO_APO);
  GPIO_output_on(GPIO_APO);
        
  /* LED1 / LED2 TAU INIT */
  TAUJ_LED2_start();
  TAUB_LED1_start();
        
  /* HPLED1 / HPLED2 PWMD INIT */
  PWMD_init(HPLED1);
  PWMD_init(HPLED2);
  PWMD_start(HPLED1);
  PWMD_start(HPLED2);
  
  /* Initialize the PWM diagnostic function */
  PWSA_init();                         /* PWSA for HPLED-Check */
  ADCA0_limit(1u, 0x19Au, 0x094u);     /* Set limits(limit channel 1) for PWSA */
  ADCA0_limit(2u, 0x19Au, 0x094u);     /* Set limits(limit channel 2) for PWSA */
  PWSA_start();                        /* Start the PWSA */

  g_System_Status.ticks = 0u;                /* Resets the counter value System_Status.ticks */
  g_System_Status.deepstop_counter = 0u;     /* Resets the counter value System_Status.deepstop_counter */
    
  while(loop_mode)
  {      
    /* Read current potentiometer values */
    g_adc0_value=ADC_read(0u);
    g_adc1_value=ADC_read(1u);

    /* Update the (HP)LED1 with value of ADCA0 and (HP)LED2 with value of ADCA1 */
    TAUJ_dr_update((u16_T)3u, (u32_T)g_adc0_value);
    TAUB_dr_update((u16_T)8u, (u32_T)g_adc1_value); 
    PWMD_duty(HPLED1, g_adc0_value/HP_LED_DIMM_FACTOR);
    PWMD_duty(HPLED2, g_adc1_value/HP_LED_DIMM_FACTOR);
    
    /* Check if any button was pressed */
    button_handler();
    temp_buttom_state=button_P0_9_status();              /* Save button status to temp variable */
    if(temp_buttom_state == button_state_short)          /* Check if button was pressed shortly */
    { 
      g_System_Status.ticks = 0u;                        /* Reset 30sTimer when Button is pressed */
      print_text("\n\n\r>>Switch to Mode 1\n\r");        /* Send ModeSwitch via UART */
      g_System_Status.current_mode = mode1;              /* Mode is now mode 1 */
      loop_mode=0u;                                      /* Exit loop mode */
    }                                  
    else if(temp_buttom_state == button_state_long)     /* Enter DeepStop after button is pressed for 3s or longer */
    {                    
      print_text("\n\n\r>>Button pressed 3s - enter DEEPSTOP\n\r");    
      while(RLIN30_get_status() == RLIN30_busy){}
      g_System_Status.current_mode = deepstop;          /* Mode is now DEEPSTOP */
      loop_mode=0u;                                     /* Exit loop mode */
    }      
    else if(g_System_Status.deepstop_counter >= DEEPSTOP_COUNT_DOWN)      /* Enter DeepStop after 30s */
    {                                        
      print_text("\n\n\r>>30s without action - enter DEEPSTOP\n\r");
      while(RLIN30_get_status() == RLIN30_busy){}
      g_System_Status.current_mode = deepstop;          /* Mode is now DEEPSTOP */
      loop_mode=0u;                                     /* Exit loop mode */
    }
	
	  /* HPLED error check after 10ms */
    if((g_HPLED1_error == not_occurred) && (g_System_Status.ticks == 10u))     /* If no error for HPLED1 occurs after 10ms */
    { 
      g_HPLED1_error_send = not_sent;                                          /* Reset the send status of HPLED1 */
    }
  
    if((g_HPLED2_error == not_occurred) && (g_System_Status.ticks == 10u))     /* If no error for HPLED2 occurs after 10ms */
    {
      g_HPLED2_error_send = not_sent;                                          /* Reset the send status of HPLED2 */
    }
    
    /* Error counters are decremented every 30ms. Thus an error has to occur 3 times in a row to execute the error handling. */
    if((g_HPLED1_error_counter > 0) && !(g_System_Status.ticks % 30))
    	{
    		g_HPLED1_error_counter--;
    	}
    	
    if((g_HPLED2_error_counter > 0) && !(g_System_Status.ticks % 30))
    	{
    		g_HPLED2_error_counter--;
    	}


    if(!(g_System_Status.ticks % STATUS_UPDATE_INTERVAL))
    {
      /* Check if the potentiometer values have changed and reset DEEPTOP timer counter if necessary*/
      range1 = range_check ((s32_T)adc0_temp,(s32_T)g_adc0_value);
      range2 = range_check ((s32_T)adc1_temp,(s32_T)g_adc1_value);
      if( (range1 || range2 ) == 1)
      { 
        g_System_Status.deepstop_counter=0u;
      }
  
      /* Write current ADC values to temp variable */
      adc0_temp=g_adc0_value;
      adc1_temp=g_adc1_value;
            
      if(RLIN30_get_status() == RLIN30_ready)
      {
        print_status();
      }
    }

      OSTM0_wait_for_overflow();              /* Wait for timer tick */
      g_System_Status.ticks++;                /* Increment global counter */
      g_System_Status.deepstop_counter++;     /* Increment DEEPSTOP counter */
    }
    /* Leave mode2 */
    
    g_System_Status.previous_mode = mode2;    /* Previous mode is mode 2 */
     
    PWSA_stop();                              /* Stop PWSA */
    
    /* Stop all PWMs of the LEDs */
    TAUJ_LED2_stop();
    TAUB_LED1_stop();
    PWMD_deinit(HPLED1);
    PWMD_deinit(HPLED2);
    
    /* Reset the error status for both HPLEDs */
    g_HPLED1_error = not_occurred;
    g_HPLED1_error_counter = 0;
    g_HPLED2_error = not_occurred;
    g_HPLED2_error_counter = 0;
    
    GPIO_deinit(GPIO_APO);      /* Deinitialize the APO Pin used for the ADC macro */
}

/*****************************************************************************
** Function:    TASKS_start
** Description: This start function handles the initializations and modes
** Parameter:   None
** Return:      None
******************************************************************************/
void TASKS_start(void)
{
  __EI();                           /* Enable all interrupts */
  button_init();                    /* Initialize the button S5 */
  RLIN30_port_init();               /* UART port initialization for user communication */
  RLIN30_init();                    /* UART initialization for user communication */
    
  /* Operating System Timer initialization */
  OSTM0_init();                     /* Initialize the OSTM */
  OSTM0_compare(40000u);            /* OSTM0 Cycle = 40000/40MHz = 1ms */
  OSTM0_start();                    /* Start the OSTM0 */
    
  AP_init();                        /* Initialize the analogue ports */
  ADC_channel_init();               /* Initialize the both channels ADCA0 and ADCA1 */
  ADC_interrupt_enable();           /* Enables the ADC error interrupts */
  TAUJ_init();                      /* Initialize the Timer Array Unit J */
  TAUB_init();                      /* Initialize the Timer Array Unit B */
  PWMDClock_init();                 /* Initialize the clock used for the PWM */
    
  /* Evaluate wake-up / reset reason */
  if(get_wakeup_factor() == 0u)     /* After hard Reset */
  {             
    g_System_Status.current_mode = mode1;
    g_System_Status.previous_mode = reset;
    print_text("\n\r>>Enter Mode 1\n\r");
  }
  else     /* After WakeUp */
  {                        
    /* Release IO Hold of all ISO ports */
    release_ioHold();
    
    /* Check WakeUp Factor an print reason */
    if((get_wakeup_factor()&(1u<<23))==(1u<<23))
    {
        print_text("\n\r>>Wake-up from DEEPSTOP by INTP Button!\n\r");
    }      
    if((get_wakeup_factor()&(1u<<14))==(1u<<14))
    {
      print_text("\n\r>>Wake-up from DEEPSTOP by LPS (analogue input)!\n\r");
    }
        
    if((get_wakeup_factor()&(1u<<19))==(1u<<19))
    {
      print_text("\n\r>>Wake-up from DEEPSTOP by LPS (digital input)!\n\r");
    }

    /* Print previous Mode via UART */
    switch(g_System_Status.previous_mode)
    {
      case mode1:
        print_text(">>Enter previous Mode 1\n\r");
        g_System_Status.current_mode = mode1;
      break;
            
      case mode2:
        print_text(">>Enter previous Mode 2\n\r");
        g_System_Status.current_mode = mode2;
      break;
            
      default:
      break;
    }
    g_System_Status.previous_mode = deepstop;
  }
      
  /* Starts the Mode-Specific ModeFunction */
  while(1)
  {
    switch(g_System_Status.current_mode)
    {
      case mode1:
        mode_1();
      break;

      case mode2:
        mode_2();
      break;

      case deepstop:
        GPIO_init_output(GPIO_APO);           /* Initialize the APO Pin for the ADC usage */
        GPIO_output_on(GPIO_APO);
            
        while(button_P0_9_read() == 0u){}     /* Continue when button is released */
        INTP12_init();                        /* ButtonInterrupt is initialized */
             
        ADC_interrupt_disable();              /* Disable the ADC error table interrupt */

        g_adc0_value=ADC_read(ADC_CHANNEL_0);
        /* Set values for upper/lower limit +-DEEPSTOP_POTI_SENSE for LPS in DEEPSTOP */
        if(g_adc0_value<=DEEPSTOP_POTI_SENSE)                                   /* If value is <= 1024 lower limit is 0(min), upper limit is +DEEPSTOP_POTI_SENSE */
        {
          ADCA0_limit(ADC_CHANNEL_0, g_adc0_value+DEEPSTOP_POTI_SENSE, 0u);
        }
        else if(g_adc0_value>=(0xfffu-DEEPSTOP_POTI_SENSE))                     /* If value is >= 3071 upper limit is 4095(max), lower limit is -DEEPSTOP_POTI_SENSE */
        {
          ADCA0_limit(ADC_CHANNEL_0, 0xfffu, g_adc0_value-DEEPSTOP_POTI_SENSE);
        }
        else
        {    
          ADCA0_limit(ADC_CHANNEL_0, g_adc0_value+DEEPSTOP_POTI_SENSE, g_adc0_value-DEEPSTOP_POTI_SENSE);     /* Else upper limit is +DEEPSTOP_POTI_SENSE and lower limit is -DEEPSTOP_POTI_SENSE */
        }
		
        GPIO_deinit(GPIO_APO);   /* Deinitialize the APO Pin used for the ADC macro */
        ADC_LPS_init();          /* Prepare ADCA0 for LPS */
                
        /* Scan LPS Digital Input and switch to AutoScanMode */
        LPS_manual_init();       /* Initialize the manual use of LPS */
        LPS_read_compare();      /* Read the values to compare with */
        LPS_manual_deinit();     /* Deinitialize the manual use of LPS */
             
        LPS_auto_init();         /* Switch to automatic mode */
        TAUJ_LPS_start();        /* Start Timer for LPS */
        
        init_DEEPSTOP_clocks();  /* Select HighSpeed IntOSC for ADC0 and LowSpeed IntOSC for TAUJ for usage in DEEPSTOP */
        TAUJ_DEEPSTOP_init();    /* Set LED2 cycle for DEEPSTOP */
        TAUJ_LED2_start();       /* LED2 blinks in DEEPSTOP */
               
        __DI();                  /* Disable Interrupts */
                
        prepare_WakeUp();        /* Reset flags and initialize WakeUp Factors */
        enter_DeepStop();        /* Enter DeepStop */
      break;
            
      default:
      break;
    }
  }
}
