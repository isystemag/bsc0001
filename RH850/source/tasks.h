/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  tasks.h                                                         */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Header file for the tasks.c                                               */
/*                                                                           */
/*===========================================================================*/
#ifndef _TASKS_H
#define _TASKS_H

/*===========================================================================*/
/* Defines */
/*===========================================================================*/
/* Define the error states */
enum error
{
    occurred,
    not_occurred
};

/* Define the send states */
enum send
{
    sent,
    not_sent
};

/* Create a new type for the different modes of the operating system */
typedef enum 
{
  reset,
  mode1,
  mode2,
  deepstop
}modes;

/* Create a new type for the system status which contains the the mode types */
typedef struct 
{
  modes current_mode;
  modes previous_mode;
  u32_T ticks;
  u32_T deepstop_counter;
}SYSTEM_Status_S;

/* Define count directions */
enum direction
{
  up,
  down
};

/*===========================================================================*/
/* Function defines */
/*===========================================================================*/
void TASKS_start(void);
void print_text(u08_T* text_array);
void print_status(void);
u08_T range_check(s32_T ref_value, s32_T new_value);

#endif
