/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  tau.c                                                           */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for the TimerArrayUnit                                        */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "device.h"
#include "tau.h"

/*===========================================================================*/
/* Functions */
/*===========================================================================*/

/*****************************************************************************
** Function:    INTTAUJ0I1
** Description: This interrupt is generated in a specified cycle for the 
**              alternating LED signal
**              It creates a call back in the Tasks source
** Parameter:   None
** Return:      None
******************************************************************************/
#if COMPILER == COMP_IAR
  #pragma vector = 0x0049
#endif
__interrupt void INTTAUJ0I1(void)
{
  PWM_TimerInt();     /* PWM_TimerInt() in tasks source code is executed */
}

/*****************************************************************************
** Function:    TAUJ_init
** Description: Initializes the TAUJ functionality for LED2, LPS and Interrupt
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUJ_init(void)
{
  /* TAU Prescaler setup */
  TAUJ0TPS = 0xfff3u;              /* Set the TAUJ0 prescaler CK0=PCLK/8=5MHz */
   
  /* TAUJ channel 0: interval timer for LPS */
  TAUJ0CMOR0 = 0x0000u;            /* Select CK0 */
  TAUJ0CMUR0 = 0x00u;              /* Interval timer mode */
  TAUJ0CDR0 = 0x1D4Bu;             /* Set interval to 500ms (0x1D4B+1/15kHz) */
  
  /* TAUJ channel 1: interval timer for interrupt generation */
  TAUJ0CMOR1 = 0x0000u;            /* Select CK0 */
  TAUJ0CMUR1 = 0x00u;              /* Interval timer mode */
  TAUJ0CDR1 = 0x00u;               /* The interval is here set to 0. This interval will be */
                                   /* Updated through the ADC conversion of the potentiometer POT1 */

  TBTAUJ0I1 = 1u;                  /* Table interrupt is enabled by setting the table bit to 1 */
  MKTAUJ0I1 = 0u;                  /* Interrupt is unmasked by setting the mas bit to 0 */
  
  
  /* TAUJ channel 2 & 3: PWM for LED2*/
  TAUJ0CMOR2 = 0x0801u;            /* Select CK0 / Channel 2 Master */
  TAUJ0CMOR3 = 0x0409u;            /* Select CK0 / Channel 3 Slave */
  TAUJ0CMUR2 = 0x00u;
  TAUJ0CMUR3 = 0x00u;
  TAUJ0CDR2 = 0x1000u;             /* The interval counter equals the maximum ADC conversion value */
  TAUJ0CDR3 = 0x0000u;             /* The duty cycle is updated after ADC conversion */
  
  TAUJ0RDE = 0x0cu;                /* Enable simultaneous rewrite */
  TAUJ0RDM = 0x00u;                /* The simultaneous rewrite trigger signal is generated when the master channel starts counting */
  TAUJ0TOE  = 0x08u;               /* Enables Independent Channel 3 Output Mode */
  TAUJ0TOM |= 0x08u;               /* Synchronous channel 3 operation */
  TAUJ0TOC &= 0x07u;               /* Set channel3 output as Operation mode */
  TAUJ0TOL &= 0x07u;               /* Set channel3 output as positive logic */
  
  TAUJ0RDT = 0x0fu;                /* Reload all modified values to the channels */
}

/*****************************************************************************
** Function:    TAUB_init
** Description: Initializes the TAUB functionality for LED1
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUB_init(void)
{
	/* TAUB Prescaler setup */  
  TAUB0TPS = 0xfff3u;              /* Set the TAUJ0 prescaler CK0=PCLK/8=5MHz */
  
  /* TAUB channel 4 & 8: PWM for LED1*/
  TAUB0CMOR6 = 0x0801u;            /* Select CK0 / Channel 6 Master */
  TAUB0CMOR8 = 0x0409u;            /* Select CK0 / Channel 8 Slave */
  TAUB0CMUR6 = 0x00u;
  TAUB0CMUR8 = 0x00u;
  TAUB0CDR6 = 0x0FFFu;             /* The interval counter equals the maximum ADC conversion value */
  TAUB0CDR8 = 0x0000u;             /* The duty cycle is updated after ADC conversion */
  
  TAUB0RDE  = 0x0140u;             /* Enable simultaneous rewrite */
  TAUB0RDM  = 0x0000u;             /* The simultaneous rewrite trigger signal is generated when the master channel starts counting */
  TAUB0TOE  = 0x0100u;             /* Enables Independent Channel 8 Output Mode */
  TAUB0TOM |= 0x0100u;             /* Synchronous channel 8 operation */
  TAUB0TOC &= 0xFEFFu;             /* Set channel 8 output as Operation mode */
  TAUB0TOL &= 0xFEFFu;             /* Set channel 8 output as positive logic */
  
  TAUB0RDT = 0x0140u;              /* Reload all modified values to the channels */
}

/*****************************************************************************
** Function:    TAUJ_LPS_start
** Description: Starts the TAUJ output for the LPS scan trigger
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUJ_LPS_start(void)
{
  TAUJ0TS = 1u;     /* Channel 0 start trigger */
}

/*****************************************************************************
** Function:    TAUJ_INT_start
** Description: Starts the TAUJ output for the TAU interrupt
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUJ_INT_start(void)
{
  TAUJ0TS = 2u;     /* Channel 1 start trigger */
}

/*****************************************************************************
** Function:    TAUJ_LED2_start
** Description: Starts the TAU output for LED2
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUJ_LED2_start(void)
{
  /* Configure P8_5(LED2) to Alt3 output TAUJ0O3 (PWM output)*/
  PMC8 |= (1u<<5);
  PM8 &= ~(1u<<5);
  PFCE8 &= ~(1u<<5);
  PFC8 &= ~(1u<<5);
  
  TAUJ0TS = 0x0cu;     /* Channel 2 & 3 start trigger */
}

/*****************************************************************************
** Function:    TAUB_LED1_start
** Description: Starts the TAUB output for LED1
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUB_LED1_start(void)
{
  /* Configure P0_11(LED1) to Alt3 output TAUB0O8 (PWM output)*/
  PFC0   &= ~(1<<11);
  PFCE0  |=  (1<<11);
  PFCAE0 &= ~(1<<11);
  PM0    &= ~(1<<11);
  PMC0   |=  (1<<11);
  
  TAUB0TS = 0x0140u;     /* Channel 7 & 8 start trigger */
}

/*****************************************************************************
** Function:    TAUJ_LPS_stop
** Description: Stops the TAUJ output for the LPS scan trigger
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUJ_LPS_stop(void)
{
  TAUJ0TT = 0x1u;     /* Channel 0 stop trigger */
}

/*****************************************************************************
** Function:    TAUJ_INT_stop
** Description: Stops the TAUJ output for the TAUJ interrupt
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUJ_INT_stop(void)
{
  TAUJ0TT = 0x2u;     /* Channel 1 stop trigger */
}

/*****************************************************************************
** Function:    TAUJ_LED2_stop
** Description: Stops the TAUJ output for LED2
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUJ_LED2_stop(void)
{
	/* Channel 2 & 3 stop trigger */
	TAUJ0TT = 0xcu;     
	
  /* P8_5(LED2) deinit */
  PMC8 &= ~(1u<<5);
  PM8 &= ~(1u<<5);
  PFCE8 &= ~(1u<<5);
  PFC8 &= ~(1u<<5);
}

/*****************************************************************************
** Function:    TAUJ_LED2_stop
** Description: Stops the TAUJ output for LED2
** Parameter:   None
** Return:      None
******************************************************************************/
void TAUB_LED1_stop(void)
{
	/* Channel 2 & 3 stop trigger */
	TAUJ0TT |= 0x0140u;     
	
  /* P0_11(LED1) deinit */
  PMC0 &= ~(1u<<11);
  PM0 &= ~(1u<<11);
  PFCE0 &= ~(1u<<11);
  PFC0 &= ~(1u<<11);
}

/*****************************************************************************
** Function:    TAUJ_dr_update
** Description: Updates the interval time of the TAUJ0 channel: 0-3
** Parameter:   Channel 0-3
**              Duty = (tau_duty+1)/CK0
** Return:      None
******************************************************************************/
void TAUJ_dr_update(u16_T tau_channel, u32_T tau_duty)
{
  switch(tau_channel)
  {
    case 0:
      TAUJ0CDR0 = tau_duty;     /* Update channel 0 */
      TAUJ0RDT = 0x0001u;
    break;
    
    case 1:
      TAUJ0CDR1 = tau_duty;     /* Update channel 1 */
      TAUJ0RDT = 0x0002u;
    break;
    
    case 2:
      TAUJ0CDR2 = tau_duty;     /* Update channel 2 */
      TAUJ0RDT = 0x0004u;
    break;
    
    case 3:
      TAUJ0CDR3 = tau_duty;     /* Update channel 3 */
      TAUJ0RDT = 0x0008u;
    break;
    
    default:
    break;
  }
}

/*****************************************************************************
** Function:    TAUB_dr_update
** Description: Updates the interval time of the TAUB0 channel: 6 or 8
** Parameter:   Channel 6 or 8
**              Duty = (tau_duty+1)/CK0
** Return:      None
******************************************************************************/
void TAUB_dr_update(u16_T tau_channel, u32_T tau_duty)
{
  switch(tau_channel)
  {
    case 6:
      TAUB0CDR6 = tau_duty;     /* Update channel 7 */
      TAUB0RDT = 0x0040u;
    break;
    
    case 8:
      TAUB0CDR8 = tau_duty;     /* Update channel 8 */
      TAUB0RDT = 0x0100u;
    break;
    
    default:
    break;
  }
}

void TAUJ_DEEPSTOP_init(void)
{
	/* TAU Prescaler setup */  
  TAUJ0TPS = 0xfff4u;              /* Set the TAUJ0 prescaler CK0=PCLK/16=15kHz */
  
  TAUJ0CDR2 = 0x752Fu;             /* The interval is 2s (0x752F+1/15KHz) */
  TAUJ0CDR3 = 0x05DCu;             /* 5% Duty (0x05DC/0x752F+1) */
  
  TAUJ0RDT = 0x0cu;                /* Reload all modified values to the channels */
}