/*===========================================================================*/
/* Project:  F1x StarterKit Sample Software                                  */
/* Module :  tau.h                                                           */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2015 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Header file for the tau.c source code                                     */
/*                                                                           */
/*===========================================================================*/
#ifndef _TAU_H
#define _TAU_H

/*===========================================================================*/
/* Defines */
/*===========================================================================*/
/* Define weak function for call back */
#pragma weak PWM_TIMERINT

/*===========================================================================*/
/* Function defines */
/*===========================================================================*/
void TAUJ_init(void);
void TAUB_init(void);
void PWM_TimerInt(void);
void TAUJ_LPS_start(void);
void TAUJ_LPS_stop(void);
void TAUB_LED1_start(void);
void TAUB_LED1_stop(void);
void TAUJ_LED2_start(void);
void TAUJ_LED2_stop(void);
void TAUJ_INT_start(void);
void TAUJ_INT_stop(void);
void TAUJ_DEEPSTOP_init(void);
void TAUJ_dr_update(u16_T tau_channel, u32_T tau_duty);
void TAUB_dr_update(u16_T tau_channel, u32_T tau_duty);
#endif