/*
  Demonstration application for iSYSTEM Training BSC0001
  Written for SAM D21 fitted to Arduino M0 Pro board.
 */
#include "Arduino.h"

unsigned int rndDelay();
void fillArray();
void myDelay(int delayTime);

static unsigned int counter = 0;
static unsigned int fastCounter = 0;
static unsigned int *pCounter = NULL;
static unsigned char testArray[10];
static unsigned char testArrayIndex = 0;
static char testArrayData = 'a';

// the setup function runs once when you press reset or power the board
void setup() {
    // initialize digital pin 13 as an output.
    pinMode(13, OUTPUT);
    
    // initialize the counter pointer
    pCounter = &counter;
}

// the loop function runs over and over again forever
void loop() {
    int newDelay = rndDelay();

    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
    myDelay(newDelay);        // wait for a second
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    myDelay(newDelay);        // wait for a second

    // Increment counter
    counter++;
    
    if (counter > 100) {
      counter = 0;
    }
    
    // Fill array
    fillArray();
}

unsigned int __attribute__ ((noinline)) rndDelay() {
    unsigned int delay;
    
    delay = random (50, 500);
    
    return delay;
}

void __attribute__ ((noinline)) fillArray() {
    // Fill array
    testArray[testArrayIndex] = testArrayData;

    ++testArrayIndex;

    if (testArrayIndex >= 10) {
        testArrayIndex = 0;
    }

    ++testArrayData;

    if (testArrayData > 'z') {
        testArrayData = 'a';
    }
}

void __attribute__ ((noinline)) myDelay(int delayTime) {

    for (int x = 0; x < delayTime; ++x) {
        fastCounter++;
        delay(1);
    }
}